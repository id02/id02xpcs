#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul 26 09:44:11 2021

@author: opid02
"""

import numpy as np
#import matplotlib.pyplot as plt

def theta_mask(shape, cx, cy, th_from, th_to):
    """ Create a theta-mask
    
    :param shape:  (x,y) size
    :param cx: x-coordinates of the center
    :param cy: y-coordinates of the center
    :param from:   minimum theta (-pi/2;pi/2)
    :param to:     maximum theta (-pi/2;pi/2)
    
    if to < from, return the inversed mask
    """
    if th_from < -np.pi/2 or th_from > np.pi/2:
        raise Exception("from must be between -pi/2 and pi/2, it is %.3f"%th_from)
        
    if th_to < -np.pi/2 or th_to > np.pi/2:
        raise Exception("to must be between -pi/2 and pi/2, it is %.3f"%th_to)
    
    Y,X = np.indices(shape)
    X = X - cx
    Y = Y - cy
    
    theta = np.arctan2(Y,X)
    theta[theta >= np.pi/2.] = theta[theta >= np.pi/2.] - np.pi
    theta[theta <= -np.pi/2.] = theta[theta <= -np.pi/2.] + np.pi
    
    if th_from <= th_to:
        mask = np.logical_and(theta >= th_from, theta <= th_to)
    else:
        mask = np.logical_or(theta >= th_from, theta <= th_to)
    
    return mask, theta

def radial_average(saxs,mask,cx,cy):
    """ Radial averaging of 2D pattern histogram method

    :param saxs: 2D array of saxs pattern
    :param mask: 2D array mask (1 is masked pixel)
    :param cx: x position of the direct beam pixel
    :param cy: y position of the direct beam pixel

    :return: 2D array first column pixel, second column average intensity
             2D array of radius in pixels
    """
    
    # Compute distance in pixels
    Y,X = np.indices(saxs.shape)
    X = X - cx
    Y = Y - cy
    q = np.float32(np.sqrt(X**2+Y**2))
    theta = np.arctan2(Y,X)
    qh = np.int16(q+0.5)#better match with data
    #qh = np.int16(q)#better match with pyfai
    
    # mask data
    #qh[mask == False] = 0
    saxs = saxs[mask]
    qh = qh[mask]
    
    # Perform radial averaging
    qmax = np.arange(0,int(qh.max())+1,1)#this is correct
    ring_brightness, radius = np.histogram(qh, weights=saxs, bins=qmax)
    rings,r = np.histogram(qh, bins=qmax)
    
    # Radial averaging data
    radi = np.zeros((len(radius)-1,2))
    radi[:,0] = radius[:-1]#(radius[:-1]+radius[1:])/2.0
    with np.errstate(divide='ignore',invalid='ignore'):
        radi[:,1] = ring_brightness/rings
    
#    plt.plot(radius[:-1], ring_brightness/rings)
#    #plt.plot(r[:-1], rings)
#    plt.xscale('log')
#    plt.yscale('log')
#    plt.show()
    
    # Rebuild the scattering pattern from 1D data
    gauss = q*0
    f1 = q-np.array(q,np.uint16)
    ind = np.array(q,np.uint16)-int(radius[0])
    val = radi[:,1]
    val = np.append(val,val*0)
    ind[ind>radius[-1]]=0
    #print(len(val),ind.max())
    gauss = val[ind+1]*f1 + val[ind]*(1.-f1)
    
    return radi, q, gauss, theta

def cftomt(d, par=16):
    """ Transformation of a correlation function 
        with linear spacing into multitau spacing 

    :param data: d array of correlation functions
    :param par: integer 8,16,32,64,128 channel legth

    :return: x correlation function with multitau spacing
    """
    
    is1d = False
    if len(d.shape) == 1:
        d = d.copy()
        d.shape = d.shape[0],1
        is1d = True
        
    if len(d.shape) > 2:
        raise ValueError('This function work only with 2D-matrix, each column is a vector')
        
    x = d[:par,:]
    tmp = d[par:,:]
    
    while tmp.shape[0] >= par:
        tmp = (tmp[:-1,:] + tmp[1:,:])/2.
        tmp = tmp[::2]
        x = np.append(x,tmp[:par,:],axis=0)
        tmp = tmp[par:,:]
        
    x = np.append(x,tmp,axis=0)
    
    if is1d:
        x.shape = x.shape[0],
        
    return x

def true_log_reduction(x, N=20):
    """ Transformation of a correlation function 
        with linear spacing into true log spacing 

    :param x: d array of correlation functions
    :param N: Max number of samples per decade 

    :return: r correlation function with log spacing
    """
    is1d = False
    if len(x.shape) == 1:
        x = x.copy()
        x.shape = x.shape[0], 1
        is1d = True
        
    if len(x.shape) > 2:
        raise ValueError('This function work only with 2D-matrix, each column is a vector')
        
    idx = np.floor(np.log10(np.arange(x.shape[0])+1)*N)
    U = np.unique(idx)
    r = np.empty((len(U),x.shape[1]))
    
    j=0
    for i in U:
        r[j,:] = np.nanmean(x[np.where(idx == i),:],axis=1)
        j+=1
        
    if is1d:
        r.shape = r.shape[0],
        
    return r


def q_mask(Q, first_q, width_q, step_q, number_q):
    """ Create q-mask from Q map
    
    :param q: Map of q
    :param first_q: lowest q in the mask
    :param width_q: width of a q-band
    :param step_q: step between 2 q-band centers
    :param number_q: Number of q-bands
    """
    
    if step_q <= width_q:
        print("Warning: bands will overlaps!")
    
    # 2D Q-mask
    qp = np.linspace(first_q,first_q+(number_q-1)*step_q,number_q)
    qmask = np.zeros(Q.shape)
    
    n = 1
    for q in qp:
        qmask[np.where(np.abs(Q-q)<=width_q/2.)] = n
        n += 1
        
    return qmask

def q_mask_log(Q, first_q, width_q, step_q, number_q, rescale_width=False):
    """ Create q-mask from Q map with logarithmic spacing
    
    :param q: Map of q
    :param first_q: lowest q in the mask
    :param width_q: width of a q-band
    :param step_q: Points per decades
    :param number_q: Number of q-bands
    """
    # 2D Q-mask
    
    lfrom = np.log10(first_q)
    lto = lfrom+(number_q-1)*(1/step_q)
    
    qp = np.logspace(lfrom,lto,number_q)
    qmask = np.zeros(Q.shape)
    
    n = 1
    for q in qp:
        if rescale_width:
            w = width_q/first_q*q/2.
        else:
            w = width_q/2.
            
        qmask[np.where(np.abs(Q-q)<=w)] = n
        n += 1
        
    return qmask
        
    