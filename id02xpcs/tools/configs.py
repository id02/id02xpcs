# -*- coding: utf-8 -*-


import logging
import configparser
import os
logger = logging.getLogger(__name__)

def process_configs(config_files):
    """
    Read all config files and return an array of config
    """
    
    rd = []
    
    trd = dict()
    trd['cx'] = ('exp_setup','dbx')
    trd['cy'] = ('exp_setup','dby')
    trd['dt'] = ('exp_setup','lagtime')
    trd['time_precision'] = ('exp_setup', 'time_precision')
    trd['first_q'] = ('exp_setup','firstq')
    trd['step_q'] = ('exp_setup','stepq')
    trd['width_q'] = ('exp_setup','widthq')
    trd['number_q'] = ('exp_setup','numberq')
    trd['distrib_q'] = ('exp_setup', 'distribq')
    trd['width_q_mode'] = ('exp_setup', 'width_q_mode')
    trd['lambdaw'] = ('exp_setup','wavelength')
    trd['distance'] = ('exp_setup','detector_distance')
    trd['th_from'] = ('exp_setup','firsttheta')
    trd['th_to'] = ('exp_setup','lasttheta')
    trd['th_width'] = ('exp_setup','widththeta')
    trd['th_N'] = ('exp_setup','numbertheta')
    
    trd['pix_size'] = ('detector','pixels')
    trd['mask_file'] = ('detector','mask')
    trd['flatfield_file'] = ('detector','flatfield')
    
    trd['correlator'] = ('correlator','method')
    trd['correlator_m'] = ('correlator','mode')
    trd['reduction'] = ('correlator','reduction')
    
    trd['outdir'] = ('data_location','result_dir')
    trd['suffix'] = ('data_location','suffix')
    
    # Invert the dict
    invtrd = dict()
    for k, v in trd.items():
        if v[0] not in invtrd:
            invtrd[v[0]] = dict()
            
        invtrd[v[0]][v[1]] = k
    
    for c in config_files:
        if not os.path.isfile(c):
            raise ValueError(f"Unable to read configuration file {c}: File do not exists.")
        elif not os.access(c, os.R_OK):
            raise ValueError(f"Unable to read configuration file {c}: Permission denied.")
            
        config = configparser.ConfigParser(interpolation=configparser.ExtendedInterpolation())
        config.read(c)
        conf = dict()
        
        for s in config.sections():
            for opt in config[s]:
                if s in invtrd and opt in invtrd[s]:
                    conf[invtrd[s][opt]] = config[s][opt]
                else:
                    logger.warning(f"Field {s}:{opt} of config file is not related to any processing parameter.")

#        for k,v in trd.items():
#            if v[0] in config and v[1] in config[v[0]]:
#                conf[k] = config[v[0]][v[1]]
#                logger.debug("Read value from %s: %s = %s", c, k, conf[k])
        
        rd += [conf,]
        
    return rd
