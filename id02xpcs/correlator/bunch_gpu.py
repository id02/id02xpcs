# -*- coding: utf-8 -*-

import logging
logging.basicConfig(format="%(levelname)s %(name)s : %(message)s")
logging.captureWarnings(True)

import numpy as np
import time
from ..tools.utils import updiv
#from dynamix.utils import updiv
#from dynamix.correlator.cuda import CublasMatMulCorrelator

try:
    from silx.math.fft.cufft import CUFFT
    import pycuda.gpuarray as garray
    from pycuda.compiler import SourceModule #, DynamicSourceModule
except ImportError:
    CUFFT = None
try:
    import skcuda.linalg as cublas
    import skcuda.misc as skmisc
except ImportError:
    cublas = None

logger = logging.getLogger(__name__)

from .common import Id02CommonCorrelator

import matplotlib.pyplot as plt

#class ID02BunchCublasMatMulCorrelator(CublasMatMulCorrelator):
class ID02BunchCublasMatMulCorrelator(Id02CommonCorrelator):

    """
    The CublasMatMulCorrelator is a CUDA-accelerated version of MatMulCorrelator.
    """

    def __init__(self, noframes):

        """
        Initialize a CUBLAS matrix multiplication correlator.
        Please refer to the documentation of BaseCorrelator for the documentation
        of each parameters.

        Extra options
        --------------
        cublas_handle: int
            If provided, use this cublas handle instead of creating a new one.
        """
        if cublas is None:
            raise ImportError("scikit-cuda is needed to use this correlator")

        #self.lag_intls = np.ascontiguousarray(lag_intls, dtype=np.float32)
        #self.nlags = len(self.lag_intls)-1
        #self.frame_timing = frame_timing
        self.nframes = noframes
        
#        super().__init__(
#            shape, len(frame_timing),
#            qmask=qmask, scale_factor=scale_factor, extra_options=extra_options
#        )
        
        self._init_cublas()
        self._compile_kernels()

        self.reset()

    def reset(self):

        self.lag_intls = None
        self.frame_timing = None

    def _init_cublas(self):
        import pycuda.autoinit
#        if "cublas_handle" in self.extra_options:
#            handle = self.extra_options["cublas_handle"]
#        else:
        
        handle = skmisc._global_cublas_handle
        if handle is None:
            cublas.init() # cublas handle + allocator
            handle = skmisc._global_cublas_handle
        self.cublas_handle = handle
        
    def _compile_kernels(self):
        
        #mod = DynamicSourceModule(
        mod = SourceModule(
            """
            
            // Rotate ttcf from (t1, t2) coordinates to (lag, age) coordinates
            __global__ void ttcf_rotate(float* ttcf, float* rttcf, int N) {
                unsigned int t1 = blockDim.x * blockIdx.x + threadIdx.x;
                unsigned int t2 = blockDim.y * blockIdx.y + threadIdx.y;
                
                if ((t1 >= N) || (t2 >= N) || (t2 > t1)) return;
                
                unsigned int t1t2_pos = t2*N+t1;
                
                unsigned int lag = t1 - t2;
                unsigned int age2 = (t1 + t2)/2;
                unsigned int lagage_pos = lag + age2*N;
                
                rttcf[lagage_pos] = ttcf[t1t2_pos];
            }
            
            // Reduce the rotated ttcf in lag time axis
            __global__ void regroup_lag(float* mat, float* lag, float* lag_intls, float* r_sum, unsigned int Nl, unsigned int Na, unsigned int rNl) {
                unsigned int lagid = blockDim.x * blockIdx.x + threadIdx.x; // This is the lag time id
                unsigned int ageid = blockDim.y * blockIdx.y + threadIdx.y; // This is the age id
                
                if(lagid >= rNl) return;
                if(ageid >= Na) return;
                
                //unsigned int nacc = 0;
                float macc = 0.;
                
                unsigned int ageoff = ageid*Nl;
                unsigned int pos;
                
                float bnd_m = lag_intls[lagid];
                float bnd_M = lag_intls[lagid+1];
                float v;
                
                for(unsigned int l=0;l<Nl;l++) {
                    pos = ageoff + l;
                    if(lag[pos] >= bnd_m && lag[pos] < bnd_M) {
                        v = mat[pos];
                        if(isfinite(v)) {
                            macc += v;
                           // nacc += N[pos];
                        }
                    }
                }
            
                unsigned int rpos = ageid*rNl+lagid;
            
                r_sum[rpos] = macc;
                //r_N[rpos] = nacc;
            }
                        
            // Reduce the rotated ttcf in age axis
            __global__ void regroup_age(float* mat, float* age, float* age_intls, float* r_sum, unsigned int Nl, unsigned int Na, unsigned int rNa) {
                unsigned int lagid = blockDim.x * blockIdx.x + threadIdx.x; // This is the lag time id
                unsigned int ageid = blockDim.y * blockIdx.y + threadIdx.y; // This is the age id
                
                if(lagid >= Nl) return;
                if(ageid >= rNa) return;
                
                //unsigned int nacc = 0;
                float macc = 0.;
                unsigned int pos;
                
                float bnd_m = age_intls[ageid];
                float bnd_M = age_intls[ageid+1];
                float v;
                unsigned int rpos = ageid*Nl+lagid;
                
                for(unsigned int a=0;a<Na;a++) {
                    pos = a*Nl + lagid;
                    if(age[pos] >= bnd_m && age[pos] < bnd_M) {
                        v = mat[pos];
                        if(isfinite(v)) {
                            macc += v;
                            //nacc += N[pos];
                        }
                    }
                }
            
                r_sum[rpos] = macc;
            }
                        
                        
            __global__ void inplace_divide(float* num, float* denom, unsigned int Nx, unsigned int Ny) {
                unsigned int x = blockDim.x * blockIdx.x + threadIdx.x;
                unsigned int y = blockDim.y * blockIdx.y + threadIdx.y;
                
                if(x >= Nx) return;
                if(y >= Ny) return;
                
                unsigned int pos = y*Nx+x;
                float mnum = num[pos];
                if(denom[pos] == 0) {
                    mnum = nanf("");
                } else {
                    mnum /= denom[pos];
                }
                
                num[pos] = mnum;
            }
               
            """
#            , options=["-g"]
        )
        
        self._ttcf_rotate = mod.get_function("ttcf_rotate")
        self._regroup_lag = mod.get_function("regroup_lag")
        self._regroup_age = mod.get_function("regroup_age")
        self._inplace_divide = mod.get_function("inplace_divide")
        
    def inplace_divide(self, d_num, d_denom):
        
        if d_num.shape != d_denom.shape:
            raise ValueError("Numerator and denominator must have the same shape!")
            
        if len(d_num.shape) != 2:
            raise ValueError("Only 2d are supported.")
            
        Nx = np.uint32(d_num.shape[0])
        Ny = np.uint32(d_num.shape[1])
            
        blocks = (32,32,1)
        grid = (updiv(Nx, self._blocks_rotate[0]),
                updiv(Ny, self._blocks_rotate[1]),
                1)
        
        #logger.debug((Nx.dtype, Ny.dtype))
        
        self._inplace_divide(d_num, d_denom, Nx, Ny, grid=grid, block=blocks)
        
        
    def _get_lag_age(self, frame_timing, lag_intls, age_intls):
        t0 = frame_timing[0]
        t1, t2 = np.meshgrid(frame_timing-t0, frame_timing-t0)
        Lag = t2 - t1
        Age = (t1 + t2)/2.
        
        olag = None
        oage = None
        
        rLag = self.r_Lags
        rlag = np.nanmean(rLag, axis=0)
        rAge = self.r_Ages
        rage = np.nanmean(rAge, axis=1)
        
        return olag, oage, Lag, Age, rlag, rlag, rage
    
    def ttcf_rotate(self, mat_in, mat_out):
        
        self._ttcf_rotate(mat_in,
                          mat_out, 
                          np.uint32(self.nframes), 
                          grid=self._grid_rotate, 
                          block=self._blocks_rotate)
        
    def _ttcf_sum_lag(self, mat_in, mat_out):
                    
        self._regroup_lag(mat_in, 
                          self.d_lags, 
                          self.d_lags_intls, 
                          mat_out, 
                          np.uint32(self.nframes), 
                          np.uint32(self.nframes), 
                          np.uint32(self.nlags), 
                          grid=self._grid_reduce_lag, 
                          block=self._blocks_reduce_lag)
        
    def _ttcf_sum_age(self, mat_in, mat_out):
                    
        self._regroup_age(mat_in,
                          self.d_ages, 
                          self.d_ages_intls, 
                          mat_out,
                          np.uint32(self.nlags), 
                          np.uint32(self.nframes), 
                          np.uint32(self.nages),
                          grid=self._grid_reduce_age, 
                          block=self._blocks_reduce_age)
        
        
    def _correlate_init(self, frame_timing, lag_intls, age_intls, roi_masks):
        
        if self.frame_timing is not None:
            if np.all(self.lag_intls == lag_intls) and np.all(self.frame_timing == frame_timing):
                return
            
        logger.debug(("LAG", lag_intls))
        logger.debug(("AGE", age_intls))
        
        self.frame_timing = frame_timing
        t0 = frame_timing[0]
        self.t2, self.t1 = np.meshgrid(self.frame_timing-t0, self.frame_timing-t0)
        self.lags = np.array(self.t2 - self.t1, dtype=np.float32)
        self.ages = np.array((self.t1 + self.t2)/2, dtype=np.float32)
            
        # LAG
        self.lag_intls = lag_intls
        self.nlags = None if lag_intls is None else len(self.lag_intls)-1
        self.d_lags_intls = garray.to_gpu(np.array(self.lag_intls, dtype=np.float32))
        self.d_lags = garray.zeros((self.nframes, self.nframes), dtype=np.float32)
        
        # AGE
        self.age_intls = age_intls
        self.nages = self.nframes if age_intls is None else len(self.age_intls)-1
        self.d_ages_intls = garray.to_gpu(np.array(self.age_intls, dtype=np.float32))
        self.d_ages = garray.zeros((self.nframes, self.nlags), dtype=np.float32)
        
        # FUNCTIONS BLOKS AND GRIDS
        self._blocks_rotate = (32,32,1)
        self._grid_rotate = (updiv(self.nframes, self._blocks_rotate[0]),
                       updiv(self.nframes, self._blocks_rotate[1]),
                       1)
        
        self._blocks_reduce_lag = (32*32,1,1)
        self._grid_reduce_lag = (updiv(self.nlags, self._blocks_reduce_lag[0]),
                           updiv(self.nframes, self._blocks_reduce_lag[1]),
                           1)
        
        self._blocks_reduce_age = (1,32*32,1)
        self._grid_reduce_age = (updiv(self.nlags, self._blocks_reduce_age[0]),
                           updiv(self.nages, self._blocks_reduce_age[1]),
                           1)
        
        # TMP arrays
        self._d_N = None
        self._d_tmp_NN = garray.zeros((self.nframes, self.nframes), dtype=np.float32)
        self._d_tmp_NN2 = garray.zeros((self.nframes, self.nframes), dtype=np.float32)
        self._d_tmp_NNl = garray.zeros((self.nframes, self.nlags), dtype=np.float32)
        self._d_tmp_NaNl = garray.zeros((self.nages, self.nlags), dtype=np.float32)
        
        # Assume that all samples are OK, needed for the dN reduction.
        self._d_tmp_NN.set(self.lags)
        self.ttcf_rotate(self._d_tmp_NN, self.d_lags)
        
        
        self._d_tmp_NN.set(self.ages)
        
        self.ttcf_rotate(self._d_tmp_NN, self._d_tmp_NN2)
        self._ttcf_sum_lag(self._d_tmp_NN2, self.d_ages)
        
        
        self._d_tmp_NN.set(np.ones(self._d_tmp_NN.shape, dtype=np.float32))
        
        self.ttcf_rotate(self._d_tmp_NN, self._d_tmp_NN2)
        self._ttcf_sum_lag(self._d_tmp_NN2, self._d_tmp_NNl)
        
#        plt.imshow(self._d_tmp_NN2.get(), origin='lower', aspect='auto')
#        plt.show()
#        
#        plt.imshow(self._d_tmp_NNl.get(), origin='lower', aspect='auto')
#        plt.show()
        
        self.inplace_divide(self.d_ages, self._d_tmp_NNl)
        
            
    def _ttcf_rotate_and_reduce(self, d_ttcf):
        
        if self._d_N is None:
#            import matplotlib.pyplot as plt
#            
#            plt.plot(self.d_ages_intls.get(),'o')
#            plt.show()
            
            # GET ACTUAL NUMBER OF CELLS
            self._d_N = garray.zeros((self.nages, self.nlags), dtype=np.float32)
            full_ttcf = d_ttcf.get()
            is_finite = np.isfinite(full_ttcf, dtype=np.bool)
            
            self._d_tmp_NN.set(np.array(is_finite, dtype=np.float32))
            self.ttcf_rotate(self._d_tmp_NN, self._d_tmp_NN2)
#            self.ttcf_rotate(self._d_tmp_NN, self._d_N)
            
#            plt.imshow(self._d_tmp_NN2.get(), origin='lower', aspect='auto')
#            plt.show()
            
            self._ttcf_sum_lag(self._d_tmp_NN2, self._d_tmp_NNl)
            
#            plt.imshow(self._d_tmp_NNl.get(), origin='lower', aspect='auto')
#            plt.show()
            
            self._ttcf_sum_age(self._d_tmp_NNl, self._d_N) # _d_N contains now the number of cells to be averaged
            
#            logger.info("TEEEEEST")
            
#            plt.(self._d_N.get(),'o')
#            plt.show()
            
            # Update lag and age according to the real average
             
            self.lags[~is_finite] = np.nan
            self.ages[~is_finite] = np.nan
             
            self._d_tmp_NN.set(self.lags)
            self.r_Lags = self._ttcf_rotate_and_reduce(self._d_tmp_NN)
            self._d_tmp_NN.set(self.ages)
            self.r_Ages = self._ttcf_rotate_and_reduce(self._d_tmp_NN)
            
            
#            plt.imshow(self.lags)
#            plt.show()
#            plt.imshow(self.ages)
#            plt.show()
            
        self.ttcf_rotate(d_ttcf, self._d_tmp_NN2)
        
        self._ttcf_sum_lag(self._d_tmp_NN2, self._d_tmp_NNl)
        self._ttcf_sum_age(self._d_tmp_NNl, self._d_tmp_NaNl)
        
        self.inplace_divide(self._d_tmp_NaNl, self._d_N)
#        self.inplace_divide(self._d_tmp_NN2, self._d_N)
        
        return self._d_tmp_NaNl.get()        
        
    def _reduce_already_done(self):
        return True
    
    def _correlate(self, frames_flat, mask):
        arr = np.ascontiguousarray(frames_flat[:, mask], dtype=np.float32)
        npix = arr.shape[1]
        
        # Pre-allocating memory for all bins might save a bit of time,
        # but would take more memory
        d_arr = garray.to_gpu(arr)
        d_ttcf = cublas.dot(d_arr, d_arr, transb="T", handle=self.cublas_handle)
        d_means = skmisc.mean(d_arr, axis=1, keepdims=True)
        d_denom_mat = cublas.dot(d_means, d_means, transb="T", handle=self.cublas_handle)
        
        self.inplace_divide(d_ttcf, d_denom_mat)
        d_ttcf /= npix
        
        ttcf = self._ttcf_rotate_and_reduce(d_ttcf)
            
        ttcf[ttcf == 0] = np.nan
        
        logger.debug(ttcf.shape)
#        import matplotlib.pyplot as plt
#        plt.plot(np.sum(~np.isnan(self.r_lags), axis=0))
#        plt.show()
        
        # BEWARE OF COUNTS! DO NOT USE NANMEAN, NANSTD, NAN in the lower triangles ARE 0!
        
        Ns = np.sum(~np.isnan(self.r_Lags), axis=0)
        
        cf   = np.nansum(ttcf, axis=0) / Ns
        stde = ((np.nansum(ttcf**2, axis=0)/Ns - cf**2)/Ns )**0.5
        #stde = np.nanstd(ttcf, axis=0)
        
        return cf, stde, ttcf