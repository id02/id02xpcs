# -*- coding: utf-8 -*-


import logging
logging.basicConfig(format="%(levelname)s %(name)s : %(message)s")
logging.captureWarnings(True)

import time, os
from datetime import datetime
import numpy as np
import h5py, hdf5plugin
import fabio, json

logger = logging.getLogger(__name__)

def convert(vals, t):
    if t == 'float':
        pval = [ float(v) if v != 'None' else None for v in vals ]
    elif t == 'int':
        pval = [ int(v) if v != 'None' else None for v in vals ]
    elif t == 'str':
        pval = [ v.decode() if v != 'None' else None for v in vals ]
    elif t == 'path':
        pval = [ '/'.join([ v.decode() for v in vals ]), ]
    else:
        raise ValueError("Unknown data_type. Should be float, int, str or path")
        
    return pval

def readField(fd, basepath, conf):
    def readSingleField(fd, basepath, field):
        if ':' in field: # This is a json dict
            fname = field.split(':')[0]
            fitem = field.split(':')[1]
            
            fval = json.loads(fd[f"{basepath}/{fname}"][()])[fitem]
        else:
            fval = fd[f"{basepath}/{field}"][()]
            
        return fval
    
    if isinstance(conf['fields'], list):
        vals = [ readSingleField(fd, basepath, f) for f in conf['fields'] ]
    else:
        vals = [ readSingleField(fd, basepath, conf['fields']), ]
    
    pval = convert(vals, conf['data_type'])
        
    if 'operation' in conf:
        if conf['operation'] == 'sum':
            return sum(pval)
    
    if 'factor' in conf:
        val = pval
        pval = [v*float(conf['factor']) for v in val]
        
    return pval[0] if len(pval) == 1 else pval



def readMeta_h5(fd, basepath, det_config):
    """
    Read and H5 file and return a dict with usefull data
    """
    
     # Read the data from H5 file
    logger.info(f"Reading metadata from: {fd.filename}:{basepath}")
    t0 = time.time()
    
    mData = dict()
    
    for k,v in det_config['data_file'].items():
        try:
            mData[k] = readField(fd, basepath, v)
        except KeyError:
            if 'default' in v:
                pval = convert([ v['default'], ], v['data_type'])
                mData[k] = pval[0] if len(pval) == 1 else pval
                logger.info(f"Field {k} not found. Using default ( {mData[k]} ).")
            else:
                raise
            
    # Display some values
    logger.info("Reading time %3.3f sec" , (time.time()-t0))
    
    return mData


def generate_timing(noframes, dt, time_precision, nobunches=None, bunch_timing=None, frames_per_bunches=None, **kwargs):
    """
    Generate the frames timing vector.
    
    Return frame_timing, and if timing is uniform.
    """
    
    intdt = np.int64(np.round(dt / time_precision))
    
    if nobunches is None:
        # Simple case: linear spacing between frames.
        return np.arange(noframes, dtype=np.int64)*intdt, True
    elif nobunches is not None and bunch_timing is not None:
        # Bunch case
        if frames_per_bunches is not None:
            if noframes != nobunches * frames_per_bunches:
                raise ValueError(f"The number of frames do not corresponds to the given timing. Frames: {noframes}, bunches: {nobunches}, fperb: {frames_per_bunches}")
        else:
            if noframes % nobunches != 0:
                raise ValueError(f"Total number of frames is not dividible by the number of bunches")
                
            frames_per_bunches = noframes // nobunches
        
        basetiming = np.arange(frames_per_bunches, dtype=np.int64)*intdt
        intbtiming = np.int64(np.round(bunch_timing / time_precision))
        
        frame_timing = np.zeros((noframes, ), dtype=np.float64)
        for i in range(nobunches):
            frame_timing[i*frames_per_bunches:(i+1)*frames_per_bunches] = basetiming + i*intbtiming
            
        return frame_timing, nobunches == 1
    
    else:
        raise ValueError("Unable to find a combination of valid parameters for timing.")
    
            
            
    
    
    
    
    


