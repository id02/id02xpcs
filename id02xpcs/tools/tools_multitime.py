# -*- coding: utf-8 -*-

import numpy as np
import logging

logger = logging.getLogger(__name__)

def log_reduction(A, lag, N=20):

    if len(A.shape) == 1: # This is cf
        bins = log_bins(lag, N)
        rlag = 10**((bins[:-1] + bins[1:])/2)
        return rlag, bins_average(A, lag, bins)
    
    elif len(A.shape) == 2: # This is TTCF
        bins = log_bins(lag.ravel(), N)
        rlag = 10**((bins[:-1] + bins[1:])/2)
        
        r = np.array((A.shape[0], rlag.shape[0]))
        
        for i in range(A.shape[0]):
            r[i,:] = bins_average(A[i,:], lag[i,:], bins)
            
        return rlag, r
            
    else:
        raise RuntimeError("Cannot handle CF matrix with dimension > 2")
    

def log_bins(lag, N):
    
    llag = np.log10(lag[lag > 0])
    
    m_llag = np.floor(np.nanmin(llag)*N)
    M_llag = np.ceil(np.nanmax(llag)*N)
    
    nlags = M_llag - m_llag
    
    M_llag /= N
    m_llag /= N
    
    logger.debug((N, nlags, m_llag, M_llag))
    
    bins = np.linspace(m_llag, M_llag, int(nlags+1))
    
    nbrs, _   = np.histogram(llag, bins=bins)
    
    while np.any(nbrs == 0):
        
        prev_was_0=False
        newbins = []
        
        for i,n in enumerate(nbrs):
            if prev_was_0:
                prev_was_0 = False
                continue
            
            if n == 0:
                newbins += [(bins[i]+bins[i+1])/2,]
                prev_was_0 = True
            else:
                newbins += [bins[i],]
                
        newbins += [bins[-1],]
                
        bins = newbins
        nbrs, _   = np.histogram(llag, bins=bins)
        
        logger.debug((nbrs, bins))
    
    return 10**np.array(bins)
    
def bins_average(A, lag, bins):
    
    summed, _ = np.histogram(lag, weights=A, bins=bins)
    nbrs, _   = np.histogram(lag, bins=bins)
    
    with np.errstate(divide='ignore',invalid='ignore'):
        return summed / nbrs
    
    

