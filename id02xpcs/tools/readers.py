# -*- coding: utf-8 -*-


import logging
logging.basicConfig(format="%(levelname)s %(name)s : %(message)s")
logging.captureWarnings(True)

import time, os
from datetime import datetime
import numpy as np
import h5py, hdf5plugin
import fabio, json

logger = logging.getLogger(__name__)


def get_meta_from_dict(f, config):
    """
    Read metadata from h5 file and related config
    """
    rData = dict()

    for k, path in config.items():
        logger.debug(path)
        if ',' in path:
            path = [ p.strip() for p in path.split(',') ]
            logger.debug(path)
        else:
            path = [path,]
        
        value = []
        for p in path:
            
            if ':' in p: # JSON dict
                
                logger.debug(f"found a JSON dataset: {p}")
                
                h5path = p.split(':')[0]
                subpath = p.split(':')[1:]
                
                dats = f.get(h5path)[()]
                dat = json.loads(dats)
                
                logger.debug(f"{dat}")
                
                for k2 in subpath:
                    if k2 in dat:
                        dat = dat[k2]
                    else:
                        logger.debug(f"{k2} not found in {dat}")
                        raise ValueError(f"{k2} not found in {dat}")
                    
                value += [ str(dat), ]
            
            elif p in f: # Direct
                value += [ f.get(p)[()]  ]
            else: # Not found
                logger.critical('Can\'t find %s in %s'%(p, f.filename))
                raise ValueError("%s not found in data file"%p)
            
        allfloat = True
        
        logger.debug(f"{k} : {value}")
            
        for i,v in enumerate(value):
            if type(v) == bytes:
                value[i] = v.decode('utf-8')
                
            try:
                value[i] = float(value[i])
            except:
                allfloat = False
                
        if k.endswith('_file'):
            value = '/'.join(value)
        elif allfloat:
            value = sum(value)
        else:
            value = ''.join([str(v) for v in value])
            
        rData[k] = value
    
    return rData

def readFile_h5(h5_file, det_config):
    """
    Read and H5 file and return a dict with usefull data
    """
    
     # Read the data from H5 file
    logger.info("Read a NeXus HDF5 file: %s", (h5_file))
    t0 = time.time()
    
    rData = None
    data = None
        
    # Read the data, read values from H5 file if not overriden by ini file
    
    MAX_TRY=10
    
    for try_count in range(MAX_TRY):
        try:
            with h5py.File(h5_file, "r", locking=False) as f:
                rData = get_meta_from_dict(f, det_config['meta_h5'])
                logger.debug(rData)
                
                # Extra specific convertions
                if 'time' in rData:
                    rData['time'] = datetime.strptime(rData['time'], det_config['config']['timeformat_h5'])
                    
                if 'lambdaw' in rData:
                    rData['lambdaw'] *= float(det_config['config']['wavelength_scaler'])
                    
                if 'scaler_file' in rData and 'meta_scalerfile' in det_config:
                    base_path = '.'
                    if '/' in h5_file:
                        base_path = '/'.join(h5_file.split('/')[:-1])
                        
                    rData['scaler_file'] = base_path+'/'+rData['scaler_file']
                        
                    if os.path.exists(rData['scaler_file']):
                        with h5py.File(rData['scaler_file'],'r', locking=False) as f2:
                            smData = get_meta_from_dict(f2, det_config['meta_scalerfile'])
                                        
                            if 'time' in smData:
                                smData['time'] = datetime.strptime(smData['time'], det_config['config']['timeformat_scalerfile'])
                                
                            if 'lambdaw' in smData:
                                smData['lambdaw'] *= float(det_config['config']['wavelength_scaler'])
                                
                        for k,v in smData.items():
                            rData[k] = v
                            
                    else:
                        logger.warning('Can\'t find scaler file. Skip.')
                
                rData['filename_base']='.'.join(h5_file.split('/')[-1].split('.')[:-1])
                logger.debug("Filename base: %s"%rData['filename_base'])
                
                # Read data after metadata, save time in case of error
                data = f.get(det_config['config']['h5_data_path'])
                
                logger.info("Loading data... (This can take a while)")
                data = np.array(data)
            
            logger.debug("File %s has been read after %i try"%(h5_file,try_count))
            break
        except BlockingIOError as e:
            if try_count < MAX_TRY-1:
                sleep_time = 3**(try_count+1)
                logger.info("Unable to open file %s yet. Will try again in %i seconds."%(h5_file, sleep_time))
                time.sleep(sleep_time)
            else: # All tentatives fail. Give up.
                logger.fatal("Unable to open file %s after %i try. Giving up."%(h5_file, MAX_TRY))
                raise e
                
        except OSError as e:
            if try_count < MAX_TRY-1:
                sleep_time = 3**(try_count+1)
                logger.info("Unable to open file %s yet. Will try again in %i seconds."%(h5_file, sleep_time))
                time.sleep(sleep_time)
            else: # All tentatives fail. Give up.
                logger.fatal("Unable to open file %s after %i try. Giving up."%(h5_file, MAX_TRY))
                raise e

    # Display some values
    logger.info("Reading time %3.3f sec" , (time.time()-t0))
    
    return (data,rData)

def readFile_h5_fd(f, det_config):
    """
    Read and H5 file and return a dict with usefull data
    """
    
     # Read the data from H5 file
    logger.info("Read a NeXus HDF5 file: %s", (f.filename))
    t0 = time.time()
    
    rData = get_meta_from_dict(f, det_config['meta_h5'])
    logger.debug(rData)
    
    # Extra specific convertions
    if 'time' in rData:
        rData['time'] = datetime.strptime(rData['time'], det_config['config']['timeformat_h5'])
        
    if 'lambdaw' in rData:
        rData['lambdaw'] *= float(det_config['config']['wavelength_scaler'])
        
    if 'scaler_file' in rData and 'meta_scalerfile' in det_config:
        base_path = '.'
        if '/' in f.filename:
            base_path = '/'.join(f.filename.split('/')[:-1])
            
        rData['scaler_file'] = base_path+'/'+rData['scaler_file']
            
        if os.path.exists(rData['scaler_file']):
            with h5py.File(rData['scaler_file'],'r', locking=False) as f2:
                smData = get_meta_from_dict(f2, det_config['meta_scalerfile'])
                            
                if 'time' in smData:
                    smData['time'] = datetime.strptime(smData['time'], det_config['config']['timeformat_scalerfile'])
                    
                if 'lambdaw' in smData:
                    smData['lambdaw'] *= float(det_config['config']['wavelength_scaler'])
                    
            for k,v in smData.items():
                rData[k] = v
                
        else:
            logger.warning('Can\'t find scaler file. Skip.')
    
    rData['filename_base']='.'.join(f.filename.split('/')[-1].split('.')[:-1])
    logger.debug("Filename base: %s"%rData['filename_base'])
    
    # Read data after metadata, save time in case of error
    data = f.get(det_config['config']['h5_data_path'])
    
    logger.info("Loading data... (This can take a while)")
    data = np.array(data)

    # Display some values
    logger.info("Total reading time %3.3f sec" , (time.time()-t0))
    
    return (data,rData)


def readFile_edf(edf_file, det_config):
    """
    This reads an EDF file, data and headers
    """
    
    data=None
    mData=dict()
    
    with fabio.open(edf_file) as f:
        data = f.data
        
        trd = det_config['meta_edf']
        
        for (k1,k2) in trd.items():
            if k2 in f.header:
                mData[k1] = f.header[k2]
                logger.debug("Read value from EDF file: %s = %s", k1, mData[k1])
                
        if 'time' in f.header:
            time_str = f.header['time_of_day'].split('.')
            
            if det_config['config']['timeformat_edf'] == 'timestamp': 
                mData['time'] = datetime.fromtimestamp(float(mData['time']))
            else:
                mData['time'] = datetime.strptime(mData['time'], det_config['config']['timeformat_edf'])
                
            logger.debug("Read time from EDF file: %s", time_str)
            
        if 'lambdaw' in mData:
            mData['lambdaw'] = float(mData['lambdaw'])*float(det_config['config']['wavelength_scaler'])
            
        if 'mask_file' in mData and mData['mask_file'][0] != '/':
             mData['mask_file'] = '%s/%s'%('/'.join(edf_file.split('/')[:-1]), mData['mask_file'])
            
        if 'flatfield_file' in mData and mData['flatfield_file'][0] != '/':
             mData['flatfield_file'] = '%s/%s'%('/'.join(edf_file.split('/')[:-1]), mData['flatfield_file'])
        
        mData['filename_base']='.'.join(edf_file.split('/')[-1].split('.')[:-1])
        logger.debug("Filename base: %s", mData['filename_base'])
        
    return (data, mData)
    
    



