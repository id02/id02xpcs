# -*- coding: utf-8 -*-

import logging
logging.basicConfig(format="%(levelname)s %(name)s : %(message)s")
logging.captureWarnings(True)

import numpy as np
import time
from dynamix.utils import updiv
from dynamix.correlator.cuda import CublasMatMulCorrelator

try:
    from silx.math.fft.cufft import CUFFT
    import pycuda.gpuarray as garray
    from pycuda.compiler import SourceModule
except ImportError:
    CUFFT = None
try:
    import skcuda.linalg as cublas
    import skcuda.misc as skmisc
except ImportError:
    cublas = None

logger = logging.getLogger(__name__)

#class ID02MultitimeCublasMatMulCorrelator(CublasMatMulCorrelator):
class ID02MultitimeCublasMatMulCorrelator():

    """
    The CublasMatMulCorrelator is a CUDA-accelerated version of MatMulCorrelator.
    """

#    def __init__(self, shape, lag_intls, frame_timing,
#                 qmask=None,
#                 scale_factor=None,
#                 extra_options={}):
    
    def __init__(self, lag_intls, frame_timing):

        """
        Initialize a CUBLAS matrix multiplication correlator.
        Please refer to the documentation of BaseCorrelator for the documentation
        of each parameters.

        Extra options
        --------------
        cublas_handle: int
            If provided, use this cublas handle instead of creating a new one.
        """
        if cublas is None:
            raise ImportError("scikit-cuda is needed to use this correlator")

        self.lag_intls = np.ascontiguousarray(lag_intls, dtype=np.float32)
        self.nlags = len(self.lag_intls)-1
        self.frame_timing = frame_timing
        
#        super().__init__(
#            shape, len(frame_timing),
#            qmask=qmask, scale_factor=scale_factor, extra_options=extra_options
#        )
        
        self._init_cublas()
        self._compile_kernels()


    def _init_cublas(self):
        import pycuda.autoinit
        if "cublas_handle" in self.extra_options:
            handle = self.extra_options["cublas_handle"]
        else:
            handle = skmisc._global_cublas_handle
            if handle is None:
                cublas.init() # cublas handle + allocator
                handle = skmisc._global_cublas_handle
        self.cublas_handle = handle
        
    def _compile_kernels(self):
        
        mod = SourceModule(
            """                
            // Compute the average, taking care of non-finite values in ttcf (happen if too low counts, some are nan)
            __global__ void avg_stde_from_ttcf(float* ttcf, float* lag, float* lag_intls, float* avg, float* stde, unsigned int N, unsigned int Nl) {
                unsigned int x = blockDim.x * blockIdx.x + threadIdx.x;
                if(x >= Nl) return;
                
                unsigned int nacc = 0;
                float macc = 0.;
                float sacc = 0.;
                
                for(unsigned int y=0;y<N*N;y++) {
                    if(lag[y] >= lag_intls[x] && lag[y] < lag_intls[x+1]) {
                        float v = ttcf[y];
                        if(isfinite(v) && v != 0.) {
                            macc += v;
                            sacc += v*v;
                            nacc += 1;
                        }
                    }
                }
                
                avg[x] = macc/(1.*nacc);
                stde[x] = ((sacc)/(1.*nacc) - avg[x]*avg[x])/(1.*nacc);
            }
            """
        )
        
        self.avg_stde_from_ttcf = mod.get_function("avg_stde_from_ttcf")
        
        self._blocks2d = (32, 32, 1)
        self._grid2d = (
            updiv(self.nframes, self._blocks2d[0]),
            updiv(self.nframes, self._blocks2d[1]),
            1
        )
        
        self._blocks1d = (32*32, 1, 1)
        self._grid1d = (
            updiv(self.nlags, self._blocks1d[0]),
            1,
            1
        )
        
        self.d_lags_intls = garray.to_gpu(self.lag_intls)
        #self.d_frame_timing = garray.to_gpu(self.frame_timing)
        
        self.t2, self.t1 = np.meshgrid(self.frame_timing, self.frame_timing)
        self.lags = np.array(self.t2 - self.t1, dtype=np.float32)
        self.ages = (self.t1 + self.t2)/2
        self.d_lags = garray.to_gpu(self.lags)
        
#        logger.debug(self.lag_intls)
#        logger.debug(self.lags)
        
        #self.d_tmp2 = garray.zeros((self.nframes, self.nframes), dtype=np.float32)
        #self.d_ttcf = garray.zeros((self.nframes, self.nlags), dtype=np.float32)
        self.d_average = garray.zeros(self.nlags, dtype=np.float32)
        self.d_variance = garray.zeros_like(self.d_average)
    
    def _correlate_matmul_cublas(self, frames_flat, mask):
        arr = np.ascontiguousarray(frames_flat[:, mask], dtype=np.float32)
        npix = arr.shape[1]
        
        # Pre-allocating memory for all bins might save a bit of time,
        # but would take more memory
        d_arr = garray.to_gpu(arr)
        d_outer = cublas.dot(d_arr, d_arr, transb="T", handle=self.cublas_handle)
        d_means = skmisc.mean(d_arr, axis=1, keepdims=True)
        d_denom_mat = cublas.dot(d_means, d_means, transb="T", handle=self.cublas_handle)
        
        d_ttcf = skmisc.divide(d_outer, d_denom_mat)
        d_ttcf /= npix
        
#         Compute average and standard error from ttcf
        self.avg_stde_from_ttcf(d_ttcf,
                                self.d_lags,
                                self.d_lags_intls,
                                self.d_average,
                                self.d_variance,
                                np.uint32(self.nframes),
                                np.uint32(self.nlags),
                                grid=self._grid1d,
                                block=self._blocks1d)
        
        ttcf = d_ttcf.get()
        logger.debug(ttcf.shape)

        ttcf[ttcf == 0] = None
        return (self.d_average.get()[1:], self.d_variance.get()[1:]**.5, ttcf)
        
    def correlate_and_save(self, frames, flatfield, cdata, bins, qmask, output):
        
#        output.create_dataset("q_index", self.bins.shape, dtype=self.bins.dtype, data=self.bins)
        output.create_dataset("q_index", bins.shape, dtype=bins.dtype, data=bins)
        
        
        ogrp = output.create_group("correlation")
        
        ogrp.attrs["NX_class"] = "NXdata"
        ogrp.attrs["axes"] = ["lag", "."]
        ogrp.attrs["signal"] = "cf"
        ogrp.attrs["error"] = "std"
        
        r_time = (self.lag_intls[2:] + self.lag_intls[1:-1])/2.
        
        ts = ogrp.create_dataset("lag", r_time.shape, dtype=np.float32, data=r_time)
        ts.attrs["interpretation"] = "scalar"
        ts.attrs["unit"] = "lag_s"
        
        res  = ogrp.create_dataset("cf", (r_time.shape[0], len(bins), ), dtype=np.float32)
        res.attrs["interpretation"] = "spectrum"
        
        dev  = ogrp.create_dataset("std", (r_time.shape[0], len(bins), ), dtype=np.float32)
        dev.attrs["interpretation"] = "scalar"
        dev.attrs["unit"] = ""
        
        trace= ogrp.create_dataset("trace", (frames.shape[0], len(bins), ), dtype=np.float32)
        trace.attrs["interpretation"] = "scalar"
        trace.attrs["unit"] = ""
        
        
        ogrptt  = output.create_group("2times_correlation")
        ogrptt.attrs["NX_class"] = "NXdata"
        ogrptt.attrs["axes"] = ["age","lag"]
        ogrptt.attrs["signal"] = "ttcf"
        
        ttcf    = ogrptt.create_dataset("ttcf", (len(bins), self.nframes, self.nframes), dtype=np.float32, chunks=(1,self.nframes, self.nframes), compression="gzip")
        ttcf.attrs["interpretation"] = "image"
        
        ogrptt.create_dataset("lag", self.lags.shape, dtype=np.float32, data=self.lags)
        ogrptt["lag"].attrs["interpretation"] = "scalar"
        ogrptt["lag"].attrs["unit"] = "lag_s"
        
        ogrptt.create_dataset("age",  self.ages.shape, dtype=np.float32, data=self.ages)
        ogrptt["age"].attrs["interpretation"] = "scalar"
        ogrptt["age"].attrs["unit"] = "age_s"
        
        ogrptt.create_dataset("t1", self.t1.shape, dtype=np.float32, data=self.t1)
        ogrptt["t1"].attrs["interpretation"] = "scalar"
        ogrptt["t1"].attrs["unit"] = "t1_s"
        
        ogrptt.create_dataset("t2", self.t2.shape, dtype=np.float32, data=self.t2)
        ogrptt["t2"].attrs["interpretation"] = "scalar"
        ogrptt["t2"].attrs["unit"] = "t2_s"

        frames = frames.reshape((self.nframes, -1))
        flatfield = flatfield.ravel()
        cdata = cdata.ravel()
        #cdata = np.mean(frames,0).ravel()
        
        t1 = time.time()
        for i, bin_value in enumerate(bins):
            
            mask = (self.qmask.ravel() == bin_value)
            
            if not np.any(mask): # Skip if no pixels in this bin (May happen because of t-masking)
                continue
            
            t0 = time.time()
            tmpr, tmpd, tmptt = self._correlate_matmul_cublas(frames, mask)
            logger.info("Correlator time %.2f seconds" , (time.time()-t0))
            
            correction=1
            fcorrection = (flatfield[mask]**2).mean()/flatfield[mask].mean()**2
            correction = (cdata[mask]**2).mean()/cdata[mask].mean()**2
            correction *= fcorrection
            
            logger.debug(f"fcorr: {fcorrection} corr: {correction}")
            
            # Compute standard deviation from ttcf
#            tmpd = np.zeros_like(tmpr)
#            
#            for j in range(tmpd.shape[0]):
#                d = np.diag(tmptt, k=j+1)
#                tmpd[j] = np.std(d)/np.sqrt(len(d))
             
            res[:,i]       = (tmpr/correction)
            dev[:,i]       = (tmpd/correction)
            trace[:,i]     = np.sum(frames[:,mask],1)
            
            tmptt /= correction
            
            ttcf[i,:,:]    = tmptt
        
        logger.info("Total correlation time: %.2f seconds", time.time()-t1)
    
    
    
    