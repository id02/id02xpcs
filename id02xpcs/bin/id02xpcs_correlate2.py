#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Aug  3 11:19:00 2021

@author: opid02
"""
import logging
logging.basicConfig(format="[%(asctime)s] %(levelname)s %(module)s:%(lineno)d : %(message)s")
logging.captureWarnings(True)

import time, os, h5py
import hdf5plugin, traceback
import numpy as np
from id02xpcs.tools.output_file import XPCSResultFileWriter
from id02xpcs.tools import tools_w as xpcs_tools
from id02xpcs.tools.readers_new import readMeta_h5, generate_timing
from id02xpcs.tools.configs import process_configs
import id02xpcs.correlator.cpu as CPUCorrelators
import id02xpcs.correlator.gpu as GPUCorrelators

from id02xpcs.tools.nexus_reader import getDefaultData
from id02xpcs.tools.utils import FileCache

from pprint import pprint

#from datetime import datetime
#import configparser
import inspect, datetime
import importlib
import yaml, json


logger = logging.getLogger(__name__)

def do_correlation(data, frame_timing, ofile, corr, roi_masks, cdata, lag_binning, age_binning=None, flatfield=None, save_ttcf=True):

    if flatfield is None:
        flatfield = np.ones_like(cdata, dtype=np.float32)
    
    nframes = np.shape(data)[0]
    
    logger.info("Number of frames %d" , nframes)
    logger.info("Number of roi %d", roi_masks.shape[0])

    cf, err, trace, lagcf, ttcf, lagtt, age = corr.correlate(data, frame_timing, flatfield, cdata, roi_masks, lag_binning, age_binning)
    
    logger.debug((lagtt.shape, age.shape, ttcf.shape))
    
    ofile.setCorrelationCurve(lagcf, cf, err)
    
    if save_ttcf:
        ofile.set2tCorrelation(lagtt, age, ttcf)
    
    
def do_SAXS_curve(avg_data, ofile, center, wavelength=None, pix_size=None, distance=None, flatfield=None, mask=None, I1=None, normfactor=None, thickness=None, lambdau='nm', thicknessu='mm'):
    
    # Compute and save the SAXS curve, return the cdata matrix needed for correlation
    
    ofile.add2DPattern(avg_data, 'frame_average_raw')
        
    if flatfield is not None:
        logger.debug("Applying flatfield")
        avg_data /= flatfield
        
    I_unit = 'A.U.'
    
    if I1 is None: 
        I1 = 1.
    if normfactor is None:
        normfactor = 1.
        
    if pix_size is not None and distance is not None:
        # Solid angle correction
        
        logger.debug(f"Doing solid angle correction")
        
        Y,X = np.indices(avg_data.shape)
        pixdist = distance**2 + ((X-center[0])*pix_size)**2 + ((Y-center[1])*pix_size)**2
        dOmega = distance*pix_size**2/pixdist**1.5
            
        logger.debug(f"distance: {distance}\n cx: {center[0]}\n cy: {center[1]}\n normfactor: {normfactor} \n trm_int: {I1}")
        
        avg_data *= normfactor/(I1*dOmega)
        
        I_unit = 'sr-1'
        
        if thickness is not None:
            I_unit = f'{thicknessu}-1'
            avg_data /= thickness
        
    ofile.add2DPattern(avg_data, 'frame_average_norm')
    
    # Perform radial averaging
    rad, r_q, cdata, theta = xpcs_tools.radial_average(avg_data, mask, center[0], center[1])
    q_unit = 'px'
    
    if wavelength is not None and pix_size is not None and distance is not None:
        # Convert pixels to q:
        rad[:,0] = 4*np.pi/wavelength*np.sin(np.arctan(pix_size*rad[:,0]/distance)/2.0)
        r_q = 4*np.pi/wavelength*np.sin(np.arctan(pix_size*r_q/distance)/2.0)
        
        q_unit = f'{lambdau}-1'
    
    # Saving
    ofile.add2DParameter(r_q, 'qmap')
    ofile.add2DParameter(theta, 'thetamap')
    ofile.add2DPattern(cdata, 'gauss')
    ofile.setSaxsCurve(rad[:,0], rad[:,1], q_unit=q_unit, I_unit=I_unit)
    
    return cdata


def generate_roi_mask(rad, theta, q, dq, th=None, dth=None, datamask=None):
    """
    Generate a single ROI mask
    """
    
    msk = np.zeros_like(rad, dtype=np.bool)
    
    msk[np.logical_and(rad > q - dq/2, rad < q + dq/2)] = True
    
    if th is not None and dth is not None:
        
        assert(th <= np.pi/2)
        assert(th > -np.pi/2)
        assert(dth > 0)
        assert(dth <= np.pi)
        
        th_1 = th - dth/2
        th_2 = th + dth/2
        
        if th_1 < -np.pi/2:
            th_1 += np.pi
                
        if th_2 > np.pi/2:
            th_2 -= np.pi
            
        theta[theta >= np.pi/2.] = theta[theta >= np.pi/2.] - np.pi
        theta[theta <= -np.pi/2.] = theta[theta <= -np.pi/2.] + np.pi
        
        if th_1 <= th_2:
            mskt = np.logical_and(theta >= th_1, theta <= th_2)
        else:
            mskt = np.logical_or(theta >= th_1, theta <= th_2)
            
        msk = np.logical_and(msk, mskt)
        
    if datamask is not None:
        msk = np.logical_and(msk, datamask)
        
    return msk

def generate_qmap_thetamap(shape, center, wavelength=None, pix_size=None, distance=None):
    
    do_unit = True
    
    if wavelength is None or pix_size is None or distance is None:
        logger.warning("Some parameters (wavelength pix_size or distance) are missing. Using PIXEL distances instead of Q!")
        do_unit = False
        
    Y,X = np.indices(shape)
    X = X - center[0]
    Y = Y - center[1]
    q = np.float32(np.sqrt(X**2+Y**2))
    theta = np.arctan2(Y,X)
    
    if do_unit:
        q = 4*np.pi/wavelength*np.sin(np.arctan(pix_size*q/distance)/2.0)
        
    return q, theta

def roi_masks_from_processing(processing, q_map, theta_map, datamask=None):
    """
    Generate the dict of ROI masks for each configuration.
    """
    
    R = {}
    
    for k, conf in processing['processing'].items():
        q_mode            = conf.get('q_distrib_mode', 'lin') # Available: lin, log
        q_min           = conf['q_min']
        q_max           = conf['q_max']
        q_N             = int(conf.get('q_N', 1))
        q_width_mode    = conf.get('q_width_mode', 'fixed') # Available: fixed, scaled
        q_width         = conf['q_width'] # in [q_unit] if fixed, in proportion of q if scaled.
        th              = conf.get('theta', None)
        dth             = conf.get('dtheta', None)
        
        roi_masks = np.empty((q_N, *q_map.shape), dtype=np.bool)
        
        if q_mode == 'lin':
            qs = np.linspace(q_min, q_max, q_N)
        elif q_mode == 'log':
            qs = np.logspace(np.log10(q_min), np.log10(q_max), q_N)
        else:
            raise ValueError("Unknown q_distrib_mode. Must be lin or log.")
            
        if q_width_mode not in ('fixed', 'scaled'):
            raise ValueError("Unknown q_width_mode. Must be fixed or scaled.")
        
        dq = q_width
        
        if th is not None and dth is not None:
            th = th * np.pi/180
            dth = dth * np.pi/180
        
        for i in range(q_N):
            
            q = qs[i]
            
            if q_width_mode == 'scaled':
                dq = q_width * q
            
            roi_masks[i,:,:] = generate_roi_mask(q_map, theta_map, q, dq, th, dth, datamask)
            
        R[k] = roi_masks
        
    return R
        
        
def loadProcessingConfig(filename):

    with open(filename, 'r') as fd: 
        return yaml.safe_load(fd)

def to_str(whatever):
    if isinstance(whatever, str):
        return whatever
    elif isinstance(whatever, bytes):
        return whatever.decode()
    else:
        return str(whatever)
    
def reduceArrayIndices(fullmask):
    
    ind = np.where(fullmask>0)
    sindy = ind[0].min()
    eindy = ind[0].max()+1
    sindx = ind[1].min()
    eindx = ind[1].max()+1
    
    return [sindx, eindx], [sindy, eindy]
    
def reduceArray(data, sindx, eindx, sindy, eindy):
    
    if len(data.shape) == 2:
        return data[sindy:eindy,sindx:eindx]
    elif len(data.shape) == 3:
        return data[:,sindy:eindy,sindx:eindx]
    else:
        raise RuntimeError("Must be a 2 or 3 dim.")

def readNexusData(filename, indx, indy):
    """
    Load the data from file, as average and frame by frame. Full data are limited to indices provided.
    """
    
    with h5py.File(filename, 'r', locking=False) as fd:
    
        # Load only useful data if a fullmask is provided.
        data = getDefaultData(fd)['data']
        logger.debug("Data chunks: %s", data.chunks)
        logger.info("Full data size is %2.2f Gigabytes" , data.nbytes/2**30)
        logger.info("Loading data may take some time...")
        
        t0 = time.time()
            
        avg_data = np.zeros(data.shape[1:], dtype=np.float32)
        red_data = np.empty((data.shape[0], indy[1]-indy[0], indx[1]-indx[0]), dtype=data.dtype)
        
        for i in range(data.shape[0]):
            avg_data += data[i,:,:]
            red_data[i,:,:] = reduceArray(data[i,:,:], *indx, *indy)
            
        avg_data /= data.shape[0]
        logger.info("Reduced data size is %2.2f Gigabytes" , red_data.size*red_data.itemsize/2**30)
        logger.info("Loading time: %.2f s", time.time()-t0)
        
        return avg_data, red_data

    
def readNexusMetadata(filename, det_config):
    """
    Load the metadata from data file.
    """
    metadata = {}
    
    with h5py.File(filename, 'r', locking=False) as fd:
    
        # Get the default DS for relative reading
        data = getDefaultData(fd)
        
        # Load metadata
        basepath = data['data'].parent.name
        
        for k, v in det_config.items():
            if 'value' in v:
                metadata[k] = v['value']
            elif 'fields' in v:
                
                allthere = True
                tmp = []
                
                for p in v['fields']:
                    path = p
                    if not os.path.isabs(path):
                        path = os.path.abspath(f"{basepath}/{path}")
                    
                    if path in fd:
                            
                        tmp += [ to_str(fd.get(path)[()]) ,]
                        
                    else:
                        allthere = False
                        logger.info(f"Missing {path} in data file, skipping {k}.")
                        break
                        
                if allthere:
                    if 'data_type' in v:
                        
                        f = 1.
                        if 'factor' in v:
                            f = float(v['factor'])
                        
                        if v['data_type'] == 'int':
                            metadata[k] = [int(t)*f for t in tmp]
                        elif v['data_type'] == 'float':
                            metadata[k] = [float(t)*f for t in tmp]
                        elif v['data_type'] == 'str':
                            metadata[k] = ';;'.join(tmp)
                        elif v['data_type'] == 'path':
                            metadata[k] = os.path.join(*tmp)
                        elif v['data_type'] == 'json':
                            metadata[k] = json.loads(tmp[0])
                            if len(tmp) > 1:
                                logger.error("Only one field allowed for json loading.")
                        elif v['data_type'] == 'isotime':
                            metadata[k] = [ "%d"%datetime.datetime.strptime(tmp[0], "%Y-%m-%dT%H:%M:%SZ").timestamp(), ]
                        else:
                            logger.error(f"Unknown converter {v['data_type']}.")
                            metadata[k] = tmp
                    else:
                        metadata[k] = tmp
                        
                    if len(metadata[k]) == 1:
                        metadata[k] = metadata[k][0]
                        
        return metadata, data['data'].shape
    
def getCorrelator(correlator_setup):
    
    modulename = correlator_setup.get('module', 'id02xpcs.correlator')
    module  = importlib.import_module(modulename)
    klassname = correlator_setup.get('class', 'Id02GPUCorrelator')
    klass = getattr(module, klassname)
    
    initprm = correlator_setup.get('init', {})
    
    return klass, initprm


def getFrameTiming(nframes, timing_info):
    
    mode        = timing_info.get('mode','equal')
    dead_time   = timing_info['dead_time']
    live_time   = timing_info['live_time']
    
    delay = dead_time + live_time
    
    if mode == 'equal':
        return np.cumsum([0.,]+[delay,]*(nframes-1))
    elif mode == 'bunches':
        nbunches = timing_info['bunches']
        bdelay = timing_info['bunch_delay']
        fperb = int(nframes/nbunches)
        
        if nbunches*fperb != nframes:
            raise ValueError("Non integer frames per bunches!")
            
        basetiming = np.arange(fperb)*delay
        
        frame_timing = np.zeros((nframes,), dtype=np.float64)
        
        if len(bdelay) == 1:
            bdelay = [bdelay[0],]*nbunches
        elif len(bdelay) != nbunches:
            raise ValueError("Number of bunches do not corresponds to number of bunch delays")
        
        last_delay = 0
        for i in range(nbunches):
            timing = basetiming + bdelay[i] + last_delay
            last_delay = timing[-1]+delay
            frame_timing[i*fperb:(i+1)*fperb] = timing
            
        return frame_timing
    else:
        raise ValueError(f"Unknown timing mode {mode}")
        
def getTimeBinning(timing, binning_conf, mint, maxt):
    
    mode = binning_conf.get('mode')
    
    if mode is None:
        raise ValueError("Please, specify the lag time binning lin or log")

    
    density = binning_conf.get('density', None)
    
    if mode == 'log':
        if density is None:
            density = 32
        
        ml = np.log10(mint)
        Ml = np.log10(maxt)
        
        N = int(np.floor((Ml - ml)*density))
        
        binning = np.logspace(ml-1/(2.*density), Ml+1./(2.*density), N+1)
        
    elif mode == 'lin':
        if density is None:
            density = 1./mint
        
        N = int(np.floor(maxt*density))
        
        binning = np.linspace(mint-1/(2.*density), maxt+1./(2.*density), N+1)
        
#        import matplotlib.pyplot as plt
#        plt.plot(binning, '.')
#        plt.show()
        
    else:
        raise ValueError(f"{mode} binning is not supported.")
    
    
    binning = np.concatenate(([-binning[0],],binning))
#    return binning    
    return cleanUselessBinning(timing, binning)
        

def cleanUselessBinning(lag, lag_binning):
     N,_ = np.histogram(lag, bins=lag_binning)
     logger.debug((lag.shape, lag_binning.shape))
     
#     import matplotlib.pyplot as plt
#     plt.plot((lag_binning[:-1] + lag_binning[1:])/2,N, '.')
#     plt.show()
     
     msk = np.concatenate(([True,], N[1:] > 0, [True,]))
     
     return lag_binning[msk]
     
     
    
def processFile(f, det_config, processing, ofilename):
    
#    correlators = {}
    meta, _shape = readNexusMetadata(f, det_config)
    
    if 'parameters' in processing:
        for k, O in processing['parameters'].items():
            meta[k] = O
            
    pprint(meta)
#        logger.debug(_shape)
    
    # Load mask and flatfield if any
    mask_filename = meta.get('mask_file')
    mask = None
    if mask_filename is not None:
        if os.path.isfile(mask_filename):
            mask = FileCache.getDataFromFile(mask_filename) == 0
        else:
            logger.error(f"Mask file {mask_filename} do not exists.")
    else:
        logger.info("No mask_file found in metadata.")
    
    if mask is None:
        mask = np.ones(_shape[1:], dtype=np.bool)
        
    flat_filename = meta.get('flatfield_file')
    flat = None
    if flat_filename is not None:
        if os.path.isfile(flat_filename):
            flat = FileCache.getDataFromFile(flat_filename)
        else:
            logger.error(f"Mask file {flat_filename} do not exists.")
    else:
        logger.info("No flatfield_file found in metadata.")
    
    if flat is None:
        flat = np.ones(_shape[1:], dtype=np.float32)
        
    datamask = np.logical_and(mask, flat > 0)
    
    # Generate the qmap, thetamap and the ROI masks.
    qmap, thmap = generate_qmap_thetamap(_shape[1:], meta.get('center'), meta.get('wavelength'), meta.get('pix_size'), meta.get('distance'))
    roi_masks = roi_masks_from_processing(processing, qmap, thmap, datamask)
    
#        logger.debug(roi_masks)
    
    # Generate the fullmask, combination of all masks
    fullmask = np.zeros(_shape[1:], dtype=np.bool)
    for k, M in roi_masks.items():
        fullmask = np.logical_or(fullmask, np.any(M, axis=0))
        
    # Get the indices for reduction
    indx, indy = reduceArrayIndices(fullmask)
    
    # Finally, load only the useful data.
    avg_data, r_data = readNexusData(f, indx, indy)
    
    logger.debug((avg_data.shape, r_data.shape))
    
#        import matplotlib.pyplot as plt
#        plt.imshow(fullmask)
#        plt.show()
    
    r_flat = reduceArray(flat, *indx, *indy)
    nframes = r_data.shape[0]
    
    # Generate frame timing
    frame_timing = meta.get('frame_timing')
    
    if frame_timing is not None and len(frame_timing) == 2:
        frame_timing = np.cumsum(np.array([np.sum(frame_timing),]*nframes))
    
    if frame_timing is None:
        if 'timing_info' in meta:
            frame_timing = getFrameTiming(nframes, meta['timing_info'])
        else:
            raise ValueError("Unable to generate frame timing. Not provided nor timing_info in meta.")
    
    logger.debug(frame_timing)
    
    corr = {}
    r_roi_masks = {}
    
    # Initialize the correlators
    for k in roi_masks:
        
        correlator_setup = processing['processing'][k].get("correlator", {})
        klass, kinit = getCorrelator(correlator_setup)
        
        corr[k] = klass(nframes, **kinit)
        r_roi_masks[k] = reduceArray(roi_masks[k], *indx, *indy)
        
#            key = f"{klass.__name__}-{nframes}"
#            if key not in correlators:
#                C = klass(nframes, **kinit)
#                correlators[key] = C
#                
#            corr[k] = correlators[key]
#            
#        pprint(correlators)    
    
    # Now, this is only processing, most linkely it will not fail, we can open the ouput file.
    with XPCSResultFileWriter(ofilename) as ofile:
        
        ofile.createEntry('entry_0000')
        
        ofile.add2DParameter(mask, 'mask')
        ofile.add2DParameter(flat, 'flatfield')
        
        if "title" not in meta:
            meta["title"] = ""
            
        if 'timestamp' in meta:
            ofile.setAcquisitionDict({'timestamp': meta['timestamp']})
            
        if 'headers' in meta:
            ofile.setAcquisitionDict(meta['headers'])
        
        for k, v in meta.items():
            if isinstance(v, str):
                ofile.addStringParameter(k, v)
            else:
                e = np.array(v)
                if e.dtype == np.dtype("O"):
                    ofile.addStringParameter(k, json.dumps(v))
                else:
                    ofile.addNumpyParameter(e, k)
                    
        ofile.addStringParameter("Parameters", json.dumps(processing))
        ofile.addStringParameter("DetConfig", json.dumps(det_config))
    
        # Doing SAXS processing first
        cdata = do_SAXS_curve(avg_data,
                      ofile,
                      meta.get('center'),
                      meta.get('wavelength'),
                      meta.get('pix_size'),
                      meta.get('distance'),
                      flat,
                      datamask,
                      meta.get('I1'),
                      meta.get('normfactor'),
                      meta.get('thickness'),
                      meta.get('wavelength_unit'),
                      meta.get('thickness_unit') )
        
        
        r_cdata = reduceArray(cdata, *indx, *indy)
#        t0 = frame_timing[0]
        frame_timing -= frame_timing[0]
#        t1, t2 = np.meshgrid(frame_timing-t0, frame_timing-t0)
        t1, t2 = np.meshgrid(frame_timing, frame_timing)
        
        logger.debug((frame_timing, t1, t2))
        
        lag = t2 - t1
        age = (t1 + t2)/2
        msk = lag >= 0
        lag = np.unique(lag[msk])
        age = np.unique(age[msk])
        
        minlag = np.min(lag[lag > 0])
        duration = np.max(lag)
        
        for k in roi_masks:
            
            q = np.zeros((roi_masks[k].shape[0]), dtype=np.float32)
            th = np.zeros_like(q)
            
            for i in range(roi_masks[k].shape[0]):
                q[i] = np.mean(qmap[roi_masks[k][i,:,:]])
                th[i] = np.mean(thmap[roi_masks[k][i,:,:]])
            
            ofile.addCorrelationKey(k,roi_masks[k], {**processing['processing'][k], 'q': q, 'theta': th})
            
            lag_binning = getTimeBinning(lag, processing['processing'][k].get('lag_binning', {'mode': 'log'}), minlag, duration)
            age_binning = getTimeBinning(age, processing['processing'][k].get('age_binning', {'mode': 'lin'}), minlag, duration)
            
            logger.debug(age_binning)
            
            do_correlation(r_data,
                           frame_timing,
                           ofile,
                           corr[k],
                           r_roi_masks[k],
                           r_cdata,
                           lag_binning,
                           age_binning,
                           r_flat,
                           processing['processing'][k].get('save_ttcf', True)
                           )
            
            del corr[k]
        
        
        
def main():
    import argparse
    
    parser = argparse.ArgumentParser(description="Process raw XPCS files and compute correlation functions")
    
    parser.add_argument('config_file', help='Configuration file for data processing.')
    parser.add_argument('-v', dest='verbose', action='store_true', default=False, help='Verbose mode (minimum message level=debug)')
    parser.add_argument('--quiet', dest='quiet', action='store_true', default=False, help='Quiet mode (minimum message level=warning)')
    parser.add_argument('--det-config', dest='det_config', action='store', default='id02', help='Detector config file, which give the path of data into h5 files default: %(default)s')
    parser.add_argument('-O', dest='outdir', action='store', default=None, help='Output directory')
    parser.add_argument('-s', dest='suffix', action='store', default=None, help='Output suffix')
    parser.add_argument('-f', dest='force', action='store_true', default=False, help='Force overriding output file.')
    parser.add_argument('data_file', action='append', nargs='+', help='Data files')
    
    args = parser.parse_args()
    
    # Set the log level of each package according to the defined verbosity
    loglevel = logging.INFO
    
    if args.verbose:
        loglevel = logging.DEBUG
    elif args.quiet:
        loglevel = logging.WARNING
        
    for name in logging.root.manager.loggerDict:
        if name.startswith('id02xpcs'):
            logging.getLogger(name).setLevel(loglevel)
    
    processings = loadProcessingConfig(args.config_file)
    
    det_conf_file = '/'.join(__file__.split('/')[:-2])+'/det-config/'+args.det_config+'.yaml'
    
    if os.path.exists(args.det_config):
        det_conf_file = args.det_config
        logger.debug("Using user-defined det-config file %s"%args.det_config)
        
    elif os.path.exists(det_conf_file):
        logger.debug("Using default det-config file %s"%det_conf_file)
    else:
        logger.debug(det_conf_file)
        logger.fatal("det-config file not found!")
        exit(-1)
    
    det_config = None
    
    with open(det_conf_file, 'r') as fd: 
        det_config = yaml.safe_load(fd)
    
    if det_config is None:
        logger.fatal('Unable to read detector configuration')
        exit(-1)
        
    for f in args.data_file[0]:
        try:
            fullpath = os.path.realpath(f)
            if os.path.isfile(fullpath):
            
                path = os.path.dirname(fullpath)
                filename = os.path.basename(fullpath)
                basename, ext = os.path.splitext(filename)
                
                opath = path if args.outdir is None else args.outdir
                suffix = 'xpcs' if args.suffix is None else args.suffix
                
                ofilename = os.path.join(opath, f"{basename}_{suffix}{ext}")
                
                if not args.force and os.path.isfile(ofilename):
                    i = 0
                    while os.path.isfile(ofilename):
                        ofilename = os.path.join(opath, f"{basename}_{suffix}.{i}{ext}")
                        i += 1
                        
                bs, of = os.path.split(ofilename)
                os.makedirs(bs, exist_ok=True)
                
                processFile(fullpath, det_config, processings, ofilename)
            
            else:
                logger.error(f"File {f} not found.")
        except Exception as e:
            traceback.print_exc() 
            logger.critical(f"Problem with {f} : "+ str(e))
            
            if not isinstance(e,ValueError) and not isinstance(e,TypeError)  :
                raise
            
     