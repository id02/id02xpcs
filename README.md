# ID02_XPCS

XPCS processing tools for ESRF/ID02 XPCS files.

This project is a specific frontend of dynamix project ( https://github.com/silx-kit/dynamix ) using ID02-specific files, defining a lot of usefull metadata into files.

## Installation

**System requirements**

id02xpcs works only with python version >= 3.6. Pip will take care of all dependencies. Dependency list can be showed in setup.py file.

It is **strongly** recommended to install id02xpcs project in a dedicated virtual environment (virtualenv) to prevent any library version mismatch issue.

```bash
pip install https://gitlab.esrf.fr/chevremo/id02xpcs/-/archive/main/id02xpcs-main.zip
```

## Usage

If you install this package in virtual environement, load virtual environement first!

### id02xpcs_correlate

Process all the data files with each ini files. Ini files examples can be found in *examples* folder.

```
$ id02xpcs_correlate --help

usage: id02xpcs_correlate [-h] --ini CONFIG_FILE [--merge] [-O OUTDIR] [-o OUTFILE] [--edf] [-v] [--quiet] [--writeover] data_file [data_file ...]

Process raw XPCS files and compute correlation functions

positional arguments:
  data_file             Data files

optional arguments:
  -h, --help            show this help message and exit
  --ini CONFIG_FILE     Configuration file for data processing. Multiple configuration files can be provided, each data will be parsed once with each config file.
  --merge               Merge data from all files before correlation
  -O OUTDIR, --output-directory OUTDIR
                        Output directory.
  -o OUTFILE, --output-filebase OUTFILE
                        Output file base name.
  --edf                 Data file are legacy EDF files (default: False)
  -v                    Verbose mode (minimum message level=debug)
  --quiet               Quiet mode (minimum message level=warning)
  --writeover           Allow to write over existing files (default: False)

```
  
### id02xpcs_auto
  
Automatically process the new files in the watched (sub)folder. This launch the id02xpcs_correlate command for each new file, taking care of not launching more than NCORES processes.

```
$ id02xpcs_auto --help  

usage: id02xpcs_auto [-h] [--dir WATCH_DIRECTORY] [-R] [-S SLEEP] [-n NCORES] [-v] [--quiet] ini_files [ini_files ...]

Automatically process raw XPCS files and compute correlation functions

positional arguments:
  ini_files             Configuration file for data processing. Multiple configuration files can be provided, each data will be parsed once with each config file.

optional arguments:
  -h, --help            show this help message and exit
  --dir WATCH_DIRECTORY
                        Directory to watch (default: .)
  -R                    Watch recursively (default: False)
  -S SLEEP, --sleep SLEEP
                        Sleep time before running (default: 10)
  -n NCORES, --cores NCORES
                        Max number of process (default: 4)
  -v                    Verbose mode (minimum message level=debug)
  --quiet               Quiet mode (minimum message level=warning)
```
                        
### id02xpcs_merge
  
Merge files containing single XPCS frame to one multiple-frame H5 file. 
  
```
$ id02xpcs_merge --help
usage: id02xpcs_merge [-h] [-v] [--quiet] [-o OUTFILE] [-O OUTDIR] [--force FORCE] data_file [data_file ...]

Merge raw XPCS files into single file

positional arguments:
  data_file             Data files

optional arguments:
  -h, --help            show this help message and exit
  -v                    Verbose mode (minimum message level=debug)
  --quiet               Quiet mode (minimum message level=warning)
  -o OUTFILE, --output OUTFILE
                        Output file.
  -O OUTDIR, --output-dir OUTDIR
                        Output directory.
  --force FORCE         Overwrite existing files.
```

  
  
