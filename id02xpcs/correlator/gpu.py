#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 11 10:57:02 2021

@author: opid02
"""

import numpy as np
import logging

try:
    import skcuda.linalg as cublas
    import skcuda.misc as skmisc
    import pycuda.gpuarray as garray
    from pycuda.compiler import SourceModule
except ImportError:
    cublas = None

from .common import Id02CommonCorrelator
from ..tools.utils import updiv

logger = logging.getLogger(__name__)

class Id02GPUCorrelator(Id02CommonCorrelator):
    """
    This is a reimplementation of CublasMatMulCorrelator, and 
    give access to all ttcf.
    
    Results are saved in the output h5 file
    """
    def __init__(self, nframes):
        super().__init__(nframes)
        self.extra_options={}
        
        self._init_cublas()
        self._compile_kernels()

    def _init_cublas(self):
        import pycuda.autoinit
        if "cublas_handle" in self.extra_options:
            handle = self.extra_options["cublas_handle"]
        else:
            handle = skmisc._global_cublas_handle
            if handle is None:
                cublas.init() # cublas handle + allocator
                handle = skmisc._global_cublas_handle
        self.cublas_handle = handle
        
    def _compile_kernels(self):
        mod = SourceModule(
            """
            // Tilt ttcf from (frames, frames) to (lag, age) ordinates
            __global__ void extract_upper_diags(float* matrix, float* diags, int N) {
                int x = blockDim.x * blockIdx.x + threadIdx.x;
                int y = blockDim.y * blockIdx.y + threadIdx.y;
                if ((x >= N) || (y >= N) || (y > x)) return;
                int pos = y*N+x;
                int my_diag = x-y;
                diags[my_diag + (x+y)/2*N] = matrix[pos];
            }
            
            // Divide each ttcf elements according to the number of elements in the diagonal, performed in (lag, age).  Elements outside left triangle are set to 0. Average is then the sum of columns.
            __global__ void trigl_avg_elms(float* matrix, float* avgs, int N) {
                int x = blockDim.x * blockIdx.x + threadIdx.x;
                int y = blockDim.y * blockIdx.y + threadIdx.y;
                if ((x >= N) || (y >= N)) return;
                int pos = y*N+x;
                int Ne = (y+1)*2;
                int Ne2 = (N-y-1)*2+1;
                if(Ne > Ne2) { Ne = Ne2; }
                if(x >= Ne) {
                    avgs[pos] = 0;
                } else {
                    avgs[pos] = matrix[pos]/(1.*(N-x));
                }
            }
            
            // Compute the var elementwise, taking care of number or elements in the diagonal. Elements outside left triangle are set to 0. Standard deviation is then the sum of columns.
            __global__ void trigl_vari_elms(float* matrix, float* vari, float* avgs, int N) {
                int x = blockDim.x * blockIdx.x + threadIdx.x;
                int y = blockDim.y * blockIdx.y + threadIdx.y;
                if ((x >= N) || (y >= N)) return;
                int pos = y*N+x;
                int Ne = (y+1)*2;
                int Ne2 = (N-y-1)*2+1;
                if(Ne > Ne2) { Ne = Ne2; }
                if(x >= Ne) {
                    vari[pos] = 0;
                } else {
                    vari[pos] = ( matrix[pos] - avgs[x] )*( matrix[pos] - avgs[x] )/(1.*(N-x)*(N-x));
                }
            }
            
            """
        )
        
        self.extract_diags_kernel = mod.get_function("extract_upper_diags")
        self.trigl_avg_elms = mod.get_function("trigl_avg_elms")
        self.trigl_vari_elms = mod.get_function("trigl_vari_elms")
        
        self._blocks = (32, 32, 1)
        self._grid = (
            updiv(self.nframes, self._blocks[0]),
            updiv(self.nframes, self._blocks[1]),
            1
        )
        self.d_ttcf = garray.zeros((self.nframes, self.nframes), dtype=np.float32)
        self.d_average = garray.zeros(self.nframes, dtype=np.float32)
        self.d_variance = garray.zeros_like(self.d_average)
    
    def _correlate_matmul_cublas(self, frames_flat, mask):
        arr = np.ascontiguousarray(frames_flat[:, mask], dtype=np.float32)
        npix = arr.shape[1]
        # Pre-allocating memory for all bins might save a bit of time,
        # but would take more memory
        d_arr = garray.to_gpu(arr)
        d_outer = cublas.dot(d_arr, d_arr, transb="T", handle=self.cublas_handle)
        d_means = skmisc.mean(d_arr, axis=1, keepdims=True)
        d_denom_mat = cublas.dot(d_means, d_means, transb="T", handle=self.cublas_handle)
        
        tmp1 = skmisc.divide(d_outer, d_denom_mat)
        tmp1 /= npix
        
        # Get TTCF in (lag, age)
        self.extract_diags_kernel(tmp1,self.d_ttcf,np.int32(self.nframes),grid=self._grid, block=self._blocks)
        
        # Average
        #self.trigl_avg_elms(self.d_ttcf,tmp1,np.int32(self.nframes),grid=self._grid, block=self._blocks)
        #skmisc.sum(tmp1, axis=0, out=self.d_average)
        
        # Variance
        #self.trigl_vari_elms(self.d_ttcf, tmp1, self.d_average, np.int32(self.nframes), grid=self._grid, block=self._blocks)
        #skmisc.sum(tmp1, axis=0, out=self.d_variance)

        ttcf = self.d_ttcf.get()
        ttcf[ttcf == 0] = None
        
#        return (self.d_average.get()[1:], self.d_variance.get()[1:]**.5, ttcf)
        
        avg = np.nansum(ttcf, axis=0)/(np.arange(ttcf.shape[0])[::-1])
        std = ((np.nansum(ttcf**2, axis=0)/(np.arange(ttcf.shape[0])[::-1]) - avg**2)/(np.arange(ttcf.shape[0])[::-1]))**0.5
        
        return avg, std, ttcf
        
        
    def _correlate(self, frames, mask):
        return self._correlate_matmul_cublas(frames, mask)
       
            
            



