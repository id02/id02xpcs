#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 11 10:57:02 2021

@author: opid02
"""

from dynamix.correlator.dense import MatMulCorrelator
from dynamix.utils import updiv
import numpy as np
import logging, time

import pycuda.driver as drv
import pycuda.gpuarray as garray
from pycuda.compiler import SourceModule
import skcuda.linalg as cublas
import skcuda.misc as skmisc

logger = logging.getLogger(__name__)

class GPUCorrelator(object):

    """
    The CublasMatMulCorrelator is a CUDA-accelerated version of MatMulCorrelator.
    """

    def __init__(self):

        """
        Initialize a CUBLAS matrix multiplication correlator.
        Please refer to the documentation of BaseCorrelator for the documentation
        of each parameters.

        Extra options
        --------------
        cublas_handle: int
            If provided, use this cublas handle instead of creating a new one.
        """
        if cublas is None:
            raise ImportError("scikit-cuda is needed to use this correlator")

        self._init_cublas()
        self._compile_kernels()
        self.nframes = 0

    def _init_cublas(self):
        import pycuda.autoinit
        handle = skmisc._global_cublas_handle
        if handle is None:
            cublas.init() # cublas handle + allocator
            handle = skmisc._global_cublas_handle
        self.cublas_handle = handle
        
    def _compile_kernels(self):
        mod = SourceModule(
            """
            // Tilt ttcf from (frames, frames) to (lag, age) ordinates
            __global__ void extract_upper_diags(float* matrix, float* diags, int N) {
                int x = blockDim.x * blockIdx.x + threadIdx.x;
                int y = blockDim.y * blockIdx.y + threadIdx.y;
                if ((x >= N) || (y >= N) || (y > x)) return;
                int pos = y*N+x;
                int my_diag = x-y;
                diags[my_diag + (x+y)/2*N] = matrix[pos];
            }
            
            // Divide each ttcf elements according to the number of elements in the diagonal, performed in (lag, age).  Elements outside left triangle are set to 0. Average is then the sum of columns.
            __global__ void trigl_avg_elms(float* matrix, float* avgs, int N) {
                int x = blockDim.x * blockIdx.x + threadIdx.x;
                int y = blockDim.y * blockIdx.y + threadIdx.y;
                if ((x >= N) || (y >= N)) return;
                int pos = y*N+x;
                int Ne = (y+1)*2;
                int Ne2 = (N-y-1)*2+1;
                if(Ne > Ne2) { Ne = Ne2; }
                if(x >= Ne) {
                    avgs[pos] = 0;
                } else {
                    avgs[pos] = matrix[pos]/(1.*(N-x));
                }
            }
            
            // Compute the var elementwise, taking care of number or elements in the diagonal. Elements outside left triangle are set to 0. Standard deviation is then the sum of columns.
            __global__ void trigl_vari_elms(float* matrix, float* vari, float* avgs, int N) {
                int x = blockDim.x * blockIdx.x + threadIdx.x;
                int y = blockDim.y * blockIdx.y + threadIdx.y;
                if ((x >= N) || (y >= N)) return;
                int pos = y*N+x;
                int Ne = (y+1)*2;
                int Ne2 = (N-y-1)*2+1;
                if(Ne > Ne2) { Ne = Ne2; }
                if(x >= Ne) {
                    vari[pos] = 0;
                } else {
                    vari[pos] = ( matrix[pos] - avgs[x] )*( matrix[pos] - avgs[x] )/(1.*(N-x)*(N-x));
                }
            }
            
            """
        )
        
        self.extract_diags_kernel = mod.get_function("extract_upper_diags")
        self.trigl_avg_elms = mod.get_function("trigl_avg_elms")
        self.trigl_vari_elms = mod.get_function("trigl_vari_elms")
        
        
    def setNFrames(self, nframes):
        
        if nframes == self.nframes:
            return
        
        self.nframes = nframes
        self._blocks = (32, 32, 1)
        self._grid = (
            updiv(self.nframes, self._blocks[0]),
            updiv(self.nframes, self._blocks[1]),
            1
        )
        self.d_ttcf = garray.zeros((self.nframes, self.nframes), dtype=np.float32)
        self.d_average = garray.zeros(self.nframes, dtype=np.float32)
        self.d_variance = garray.zeros_like(self.d_average)
    
    def correlate(self, frames_flat):
        
        t0 = time.perf_counter()
        
        arr = np.ascontiguousarray(frames_flat, dtype=np.float32)
        npix = arr.shape[1]
        
        logger.debug('npix=%i, nframes=%i, self.nframes=%i'%(npix, arr.shape[0],self.nframes))
        
        t1 = time.perf_counter()
        
        # Pre-allocating memory for all bins might save a bit of time,
        # but would take more memory
        d_arr = garray.to_gpu(arr)
        
        t2 = time.perf_counter()
        
        d_outer = cublas.dot(d_arr, d_arr, transb="T", handle=self.cublas_handle)
        d_means = skmisc.mean(d_arr, axis=1, keepdims=True)
        d_denom_mat = cublas.dot(d_means, d_means, transb="T", handle=self.cublas_handle)
        
        t3 = time.perf_counter()
        
        tmp1 = skmisc.divide(d_outer, d_denom_mat)
        tmp1 /= npix
        
        t4 = time.perf_counter()
        
        # Get TTCF in (lag, age)
        self.extract_diags_kernel(tmp1,self.d_ttcf,np.int32(self.nframes),grid=self._grid, block=self._blocks)
        
        t5 = time.perf_counter()
        
        # Average
#        self.trigl_avg_elms(self.d_ttcf,tmp1,np.int32(self.nframes),grid=self._grid, block=self._blocks)
#        skmisc.sum(tmp1, axis=0, out=self.d_average)
        
        t6 = time.perf_counter()
        
        # Variance
#        self.trigl_vari_elms(self.d_ttcf, tmp1, self.d_average, np.int32(self.nframes), grid=self._grid, block=self._blocks)
#        skmisc.sum(tmp1, axis=0, out=self.d_variance)
        
        t7 = time.perf_counter()

        ttcf=None
        avg=None
        stde=None
        ttcf = self.d_ttcf.get()
        
        t8 = time.perf_counter()
        
#        avg = self.d_average.get()[1:]
#        stde= self.d_variance.get()[1:]**.5
        
        t9 = time.perf_counter()
        
        timing = {'00_to_continuous': t1 - t0,
                  '01_to_gpu': t2 - t1,
                  '02_dot': t3 - t2,
                  '03_divide': t4 - t3,
                  '04_turn': t5 - t4,
                  '05_average': t6 - t5,
                  '06_variance': t7 - t6,
                  '07_ttcf_from_gpu': t8 - t7,
                  '08_avg_std_from_gpu': t9 - t8,
                  'total': t9 - t0
                  }
        
        logger.debug(timing)
        
        return timing, (avg, stde , ttcf)
    
class GPUCorrelator2(object):

    """
    The CublasMatMulCorrelator is a CUDA-accelerated version of MatMulCorrelator.
    """

    def __init__(self):

        """
        Initialize a CUBLAS matrix multiplication correlator.
        Please refer to the documentation of BaseCorrelator for the documentation
        of each parameters.

        Extra options
        --------------
        cublas_handle: int
            If provided, use this cublas handle instead of creating a new one.
        """
        if cublas is None:
            raise ImportError("scikit-cuda is needed to use this correlator")

        self._init_cublas()
        self._compile_kernels()
        self.nframes = 0

    def _init_cublas(self):
        import pycuda.autoinit
        handle = skmisc._global_cublas_handle
        if handle is None:
            cublas.init() # cublas handle + allocator
            handle = skmisc._global_cublas_handle
        self.cublas_handle = handle
        
    def _compile_kernels(self):
        mod = SourceModule(
            """
            // Compute the tilted ttcf from time series
            __global__ void ttcf(float* ttcf, const float* data, int Npix, int Nframes) {
                // matr position
                int x = blockDim.x * blockIdx.x + threadIdx.x;
                int y = blockDim.y * blockIdx.y + threadIdx.y;
                if ((x >= Nframes) || (y >= Nframes) || (y > x)) return;
                
                // Loop over matrix to get numerator and denominators
                float num = 0, den1 = 0, den2 = 0;
                
                for(int k=0;k<Npix;k++) {
                    num  += data[x*Npix+k]*data[y*Npix+k];
                    den1 += data[x*Npix+k];
                    den2 += data[y*Npix+k];
                }
                
                // normal and tilted position
                //int pos = y*Nframes+x;
                int my_diag = x-y;
                
                // Write to tilted output matrix
                ttcf[my_diag + (x+y)/2*Nframes] = num/(den1*den2)*Npix;
            }
            
            // Return the averaged correlation function and the standard error
            __global__ void avg_and_sqstde(float* avg, float* stde, const float* matrix, int N) {
                int x = blockDim.x * blockIdx.x + threadIdx.x;
                
                float val = 0;
                
                for(int y=0;y<N;y++) {
                    int Ne = (y+1)*2;
                    int Ne2 = (N-y-1)*2+1;
                    if(Ne > Ne2) { Ne = Ne2; }
                    if(x < Ne) { val += matrix[y*N+x]; }
                }
                
                avg[x] = val / (1.*(N-x));
                
                val = 0;
                
                for(int y=0;y<N;y++) {
                    int Ne = (y+1)*2;
                    int Ne2 = (N-y-1)*2+1;
                    if(Ne > Ne2) { Ne = Ne2; }
                    if(x < Ne) { val += (matrix[y*N+x]-avg[x])*(matrix[y*N+x]-avg[x]); }
                }
                
                stde[x] = val / (1.*((N-x)*(N-x)));
                
            }            
            """
        )
        
        self.ttcf = mod.get_function("ttcf")
        self.avg_and_sqstde = mod.get_function("avg_and_sqstde")
        
        
    def setNFrames(self, nframes):
        
        if nframes == self.nframes:
            return
        
        self.nframes = nframes
        self._blocks = (32, 32, 1)
        self._grid = (
            updiv(self.nframes, self._blocks[0]),
            updiv(self.nframes, self._blocks[1]),
            1
        )
        
        # GPU arrays
        self.d_ttcf = garray.zeros((self.nframes, self.nframes), dtype=np.float32)
        self.d_average = garray.zeros(self.nframes, dtype=np.float32)
        self.d_variance = garray.zeros_like(self.d_average)
        
        # Pinned memory 
#        self.m_stream = drv.Stream()
#        self.p_ttcf = drv.pagelocked_empty((self.nframes, self.nframes), dtype=np.float32)
#        self.p_avg = drv.pagelocked_empty((self.nframes, ), dtype=np.float32)
#        self.p_stde = drv.pagelocked_empty((self.nframes, ), dtype=np.float32)
    
    def correlate(self, frames_flat):
        
        t0 = time.perf_counter()
        
        arr = np.ascontiguousarray(frames_flat, dtype=np.float32)
        npix = arr.shape[1]
        
        logger.debug('npix=%i, nframes=%i, self.nframes=%i'%(npix, arr.shape[0],self.nframes))
        
        t1 = time.perf_counter()
        
        # Pre-allocating memory for all bins might save a bit of time,
        # but would take more memory
        d_arr = garray.to_gpu(arr)
        
        t2 = time.perf_counter()
        
        
        # Get TTCF in (lag, age)
        self.ttcf(self.d_ttcf, d_arr, np.int32(npix), np.int32(self.nframes),grid=self._grid, block=self._blocks)
        #drv.memcpy_dtoh_async(self.p_ttcf, self.d_ttcf.ptr, self.m_stream)
        
        t3 = time.perf_counter()
        
        # Get average and stde
        self.avg_and_sqstde(self.d_average, self.d_variance, self.d_ttcf, np.int32(self.nframes), grid=(updiv(self.nframes, 32),1,1), block=(32,1,1))
        #drv.memcpy_dtoh_async(self.p_avg, self.d_average.ptr, self.m_stream)
        #drv.memcpy_dtoh_async(self.p_stde, self.d_variance.ptr, self.m_stream)
        
        t4 = time.perf_counter()

        ttcf = None
        avg = None
        stde = None
        ttcf = self.d_ttcf.get()
#        
#        t5 = time.perf_counter()
##        
#        avg = self.d_average.get()[1:]
#        stde= self.d_variance.get()[1:]**.5
        
        t5 = time.perf_counter()
        
        timing = {'00_to_continuous': t1 - t0,
                  '01_to_gpu': t2 - t1,
                  '02_ttcf': t3 - t2,
                  '03_avg_std': t4 - t3,
                  '04_avg_std_from_gpu': t5 - t4,
                  'total': t5 - t0
                  }
        
        logger.debug(timing)
        
        #return timing, (self.p_avg[1:], self.p_stde[1:]**.5 , self.p_ttcf )
        return timing, (avg, stde , ttcf )


   
class GPUCorrelator3(object):

    """
    The CublasMatMulCorrelator is a CUDA-accelerated version of MatMulCorrelator.
    """

    def __init__(self):

        """
        Initialize a CUBLAS matrix multiplication correlator.
        Please refer to the documentation of BaseCorrelator for the documentation
        of each parameters.

        Extra options
        --------------
        cublas_handle: int
            If provided, use this cublas handle instead of creating a new one.
        """
        if cublas is None:
            raise ImportError("scikit-cuda is needed to use this correlator")

        self._init_cublas()
        self._compile_kernels()
        self.nframes = 0

    def _init_cublas(self):
        import pycuda.autoinit
        handle = skmisc._global_cublas_handle
        if handle is None:
            cublas.init() # cublas handle + allocator
            handle = skmisc._global_cublas_handle
        self.cublas_handle = handle
        
    def _compile_kernels(self):
        mod = SourceModule(
            """
            
            #define N_PIX 128
            #define N_FRAMES 32
            
            // Compute the tilted ttcf from time series
            __global__ void ttcf(float* ttcf, const float* data, int Npix, int Nframes) {
            
                // Declare shared memory
                __shared__ float A[N_FRAMES][N_PIX], B[N_FRAMES][N_PIX];
            
                // matr position
                int x = blockDim.x * blockIdx.x + threadIdx.x;
                int y = blockDim.y * blockIdx.y + threadIdx.y;
                
                
                // Load in shared memory
                if (threadIdx.x == 0) {
                    for(int k=0;k<Npix;k++) {
                        A[threadIdx.y][k] = data[y*Npix+k];
                    }
                }
                
                if (threadIdx.y == 0) {
                    for(int k=0;k<Npix;k++) {
                        B[threadIdx.x][k] = data[x*Npix+k];
                    }
                }
                
                if ((x >= Nframes) || (y >= Nframes) || (y > x)) return;   
                __syncthreads();            
                
                // Loop over matrix to get numerator and denominators
                float num = 0, den1 = 0, den2 = 0;
                
                for(int k=0;k<Npix;k++) {
                    num  += A[threadIdx.y][k]*B[threadIdx.x][k];
                    den1 += A[threadIdx.y][k];
                    den2 += B[threadIdx.x][k];
                }
                
                // normal and tilted position
                //int pos = y*Nframes+x;
                int my_diag = x-y;
                
                // Write to tilted output matrix
                ttcf[my_diag + (x+y)/2*Nframes] = num/(den1*den2)*Npix;
                
                //ttcf[y*Nframes+x] =  num/(den1*den2)*Npix;
            }
            
            // Return the averaged correlation function and the standard error
            __global__ void avg_and_sqstde(float* avg, float* stde, const float* matrix, int N) {
                int x = blockDim.x * blockIdx.x + threadIdx.x;
                
                float val = 0;
                
                for(int y=0;y<N;y++) {
                    int Ne = (y+1)*2;
                    int Ne2 = (N-y-1)*2+1;
                    if(Ne > Ne2) { Ne = Ne2; }
                    if(x < Ne) { val += matrix[y*N+x]; }
                }
                
                avg[x] = val / (1.*(N-x));
                
                val = 0;
                
                for(int y=0;y<N;y++) {
                    int Ne = (y+1)*2;
                    int Ne2 = (N-y-1)*2+1;
                    if(Ne > Ne2) { Ne = Ne2; }
                    if(x < Ne) { val += (matrix[y*N+x]-avg[x])*(matrix[y*N+x]-avg[x]); }
                }
                
                stde[x] = val / (1.*((N-x)*(N-x)));
                
            }            
            """
        )
        
        self.ttcf = mod.get_function("ttcf")
        self.avg_and_sqstde = mod.get_function("avg_and_sqstde")
        
        
    def setNFrames(self, nframes):
        
        if nframes == self.nframes:
            return
        
        self.nframes = nframes
        self._blocks = (32, 32, 1)
        self._grid = (
            updiv(self.nframes, self._blocks[0]),
            updiv(self.nframes, self._blocks[1]),
            1
        )
        
        # GPU arrays
        self.d_ttcf = garray.zeros((self.nframes, self.nframes), dtype=np.float32)
        self.d_average = garray.zeros(self.nframes, dtype=np.float32)
        self.d_variance = garray.zeros_like(self.d_average)
    
    def correlate(self, frames_flat):
        
        t0 = time.perf_counter()
        
        arr = np.ascontiguousarray(frames_flat, dtype=np.float32)
        npix = arr.shape[1]
        
        logger.debug('npix=%i, nframes=%i, self.nframes=%i'%(npix, arr.shape[0],self.nframes))
        
        t1 = time.perf_counter()
        
        # Pre-allocating memory for all bins might save a bit of time,
        # but would take more memory
        d_arr = garray.to_gpu(arr)
        
        t2 = time.perf_counter()
        
        
        # Get TTCF in (lag, age)
        self.ttcf(self.d_ttcf, d_arr, np.int32(npix), np.int32(self.nframes),grid=self._grid, block=self._blocks)
        #drv.memcpy_dtoh_async(self.p_ttcf, self.d_ttcf.ptr, self.m_stream)
        
        t3 = time.perf_counter()
        
        # Get average and stde
        #self.avg_and_sqstde(self.d_average, self.d_variance, self.d_ttcf, np.int32(self.nframes), grid=(updiv(self.nframes, 32),1,1), block=(32,1,1))
        #drv.memcpy_dtoh_async(self.p_avg, self.d_average.ptr, self.m_stream)
        #drv.memcpy_dtoh_async(self.p_stde, self.d_variance.ptr, self.m_stream)
        
        t4 = time.perf_counter()

        ttcf = None
        avg = None
        stde = None
#        
#        t5 = time.perf_counter()
#        
#        avg = self.d_average.get()[1:]
#        stde= self.d_variance.get()[1:]**.5
        
        t5 = time.perf_counter()
        
#        ttcf[ttcf == 0] = None
        #self.m_stream.synchronize()
        ttcf = self.d_ttcf.get()
#        
        t6 = time.perf_counter()
        
        timing = {'00_to_continuous': t1 - t0,
                  '01_to_gpu': t2 - t1,
                  '02_ttcf': t3 - t2,
                  '03_avg_std': t4 - t3,
                  '04_avg_std_from_gpu': t5 - t4,
                  '05_ttcf_from_gpu': t6 - t5,
                  'total': t6 - t0
                  }
        
        logger.debug(timing)
        
        #return timing, (self.p_avg[1:], self.p_stde[1:]**.5 , self.p_ttcf )
        return timing, (avg, stde , ttcf )

