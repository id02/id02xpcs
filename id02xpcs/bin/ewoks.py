# -*- coding: utf-8 -*-


from .id02xpcs_correlate2 import processFile, loadProcessingConfig
from ewokscore import Task
import logging, os, yaml

logger = logging.getLogger(__name__)

class XPCSFileProcessing(Task,
                         input_names=['input_file',
                                      'output_file',
                                      'processing_config'
                                      ],
                         optional_input_names=['detector_config'],
                         output_names=[]):
    
    
    """
    This task process a single XPCS file
    """
    
    def run(self):
        
        det_conffile = self.get_input_value('detector_config', 'id02')
        
        det_conf_file = '/'.join(__file__.split('/')[:-2])+'/det-config/'+det_conffile+'.yaml'
    
        if os.path.exists(det_conffile):
            det_conf_file = det_conffile
            logger.debug("Using user-defined det-config file %s"%det_conffile)
            
        elif os.path.exists(det_conf_file):
            logger.debug("Using default det-config file %s"%det_conf_file)
        else:
            logger.debug(det_conf_file)
            logger.fatal("det-config file not found!")
            exit(-1)
        
        det_config = None
        
        with open(det_conf_file, 'r') as fd: 
            det_config = yaml.safe_load(fd)
        
        if det_config is None:
            logger.fatal('Unable to read detector configuration')
            exit(-1)
            

        processing = loadProcessingConfig(self.inputs['processing_config'])
        
        ofilename = self.inputs['output_file']
        odir = os.path.dirname(os.path.abspath(ofilename))
        
        os.makedirs(odir, exist_ok=True)
        
        processFile(self.inputs['input_file'], det_config, processing, ofilename)



