#!/usr/bin/env python3
# -*- coding: utf-8 -*-


"""

Correlators benchmark

"""

import logging
logging.basicConfig(format="%(levelname)s %(name)s : %(message)s")
logging.captureWarnings(True)

from id02xpcs.benchmark.cpu import CPUCorrelator
from id02xpcs.benchmark.gpu import *

import numpy as np

import matplotlib.pyplot as plt

logger = logging.getLogger(__name__)


def check():

    loglevel = logging.DEBUG
        
    for name in logging.root.manager.loggerDict:
        if name.startswith('id02xpcs'):
            logging.getLogger(name).setLevel(loglevel)
    
    with np.load('data.npz') as f:
        data_serie = f['data']
    
    correlators = {
                   #'cpu': CPUCorrelator(),
                   'gpu': GPUCorrelator(),
                   'gpu2': GPUCorrelator2(),
                   'gpu3': GPUCorrelator3(),
                   }
    
    f1 = plt.figure()
    
    for cid, c in correlators.items():
        c.setNFrames(data_serie.shape[0])
        tm, r = c.correlate(data_serie[:,:128])
        
        plt.figure(f1)
       # plt.errorbar(np.arange(1,1000), r[0], r[1])
        plt.plot(r[0],'o') 
       
        plt.figure()
        plt.contourf(r[2])
        
    plt.show()

def main():

    loglevel = logging.INFO
        
    for name in logging.root.manager.loggerDict:
        if name.startswith('id02xpcs'):
            logging.getLogger(name).setLevel(loglevel)
    
    time_length = [10,16,25,40,63,100,160,250,400,630,1000,1600,2500,4000,6300,10000,16000,25000]
    #N_pix = [10,16,25,40,63,100]
    
    time_length = [630,1000,1600,2500,4000,6300]
    N_pix = [100]
    
    time_length.reverse()
    
    # Generate once a data serie
    data_serie = np.random.randint(1,255,(np.max(time_length),np.max(N_pix)),np.uint8)
    
    correlators = {
                   #'cpu': CPUCorrelator(),
                   'gpu': GPUCorrelator(),
                   #'gpu2': GPUCorrelator2(),
                   'gpu3': GPUCorrelator3(),
                   }
    
    symbols = {
            'cpu': 'o',
            'gpu': 'd',
            'gpu2': '^',
            'gpu3': 's',
            }
    
    # Do first correlation for some initialization
    for c in correlators:
        correlators[c].setNFrames(100)
        correlators[c].correlate(data_serie[:100,:100])
    
    ndo = 10
    
    times = np.zeros((len(time_length), len(N_pix), len(correlators), ndo))
    rep_time = dict()
    
    for tid, tl in enumerate(time_length):
        if tid not in rep_time:
            rep_time[tid] = dict()
        for nid, n in enumerate(N_pix):
            if nid not in rep_time[tid]:
                rep_time[tid][nid] = dict()
            for cid, c in enumerate(correlators):
                if cid not in rep_time[tid][nid]:
                    rep_time[tid][nid][cid] = []
                    
                corr = correlators[c]
                corr.setNFrames(tl)
                
                for i in range(ndo):
                    timing, data = corr.correlate(data_serie[:tl, :n])
                    
                    times[tid, nid, cid, i] = timing['total']
                    rep_time[tid][nid][cid] += [timing,]
    
    
    plt.figure()
    exec_time = np.mean(times, axis=3)
    exec_time_std = np.std(times, axis=3)
    for cid, c in enumerate(correlators):
        plt.gca().set_prop_cycle(None)
        for nid, n in enumerate(N_pix):
            plt.errorbar(time_length, exec_time[:,nid, cid], exec_time_std[:,nid, cid], None, symbols[c], label='Npix=%i, correlator=%s'%(n,c))
    
    plt.legend()
    plt.xscale('log')
    plt.yscale('log')
    
    plt.xlabel('Number of frames')
    plt.ylabel('Execution time')
    
#    
#    for nid, n in enumerate(N_pix):
#        for cid, c in enumerate(correlators):
#            rep_data = dict()
#            for tid, tl in enumerate(time_length):
#                dats = dict()
#                for d in rep_time[tid][nid][cid]:
#                    for k,v in d.items():
#                        if k not in dats:
#                            dats[k] = []
#                            
#                        dats[k] += [v,]
#                        
#                for k,v in dats.items():
#                    if k not in rep_data:
#                        rep_data[k] = np.zeros((len(time_length),2))
#                        
#                    rep_data[k][tid,:] = [np.mean(v), np.std(v)]
#                    
#            plt.figure()
#            
#            for k,v in rep_data.items():
#                plt.errorbar(time_length, v[:,0], v[:,1],None,'o',label=k)
#                
#            plt.title('Npix=%i, correlator=%s'%(n,c))
#            
#            plt.legend()
#            plt.xscale('log')
#            plt.yscale('log')
    
    np.savez('res.npz', exec_time=exec_time, exec_time_std=exec_time_std, times=times, correlators=list(correlators.keys()))
    plt.savefig('res.png')
    
    plt.show()
    
if __name__ == '__main__':
    #check()
    main()


