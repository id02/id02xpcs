# -*- coding: utf-8 -*-

import h5py, hdf5plugin
import os, sys
import datetime as dt
import numpy as np
import json


import logging
logger = logging.getLogger(__name__)

class XPCSResultFileException(Exception):
    pass


class XPCSResultFileWriter(object):
    """
    This class allow to create an XPCS result file in Nexus format.
    
    The file is created on the fly.
    """
    
    def __init__(self, filename):
        """
        Class contrustor
        
        :param filename: The file path
        """
        
        self.__isOpen = False
        self.__filename = filename
        self.__fd = None
        self.__entry = None
        self.__corrKey = None
        
    def __enter__(self):
        """
        Open the file on with statement
        """
        
        self.open()
        return self
    
    def __exit__(self, *args):
        """
        Close the file at the end of with statement
        """
        
        self.close()
        
    def __del__(self):
        """
        Ensure file is closed when deleting the object
        """
        
        self.close()
        
    def open(self, mode='w'):
        """
        Open the file
        
        :param mode: Mode passed to h5py
        """
        
        self.__fd = h5py.File(self.__filename, mode)
        self.__isOpen = True
        self.__entry = None
        self.__corrKey = None
        
    def close(self):
        """
        Close the file
        """
        
        if self.__isOpen:
            self.__fd.close()
            self.__fd = None
            self.__isOpen = False
            
    def _checkOpen(self):
        """
        Check that the file is opened and valid
        """
        
        if not self.__isOpen or self.__fd is None:
            raise XPCSResultFileException("The file is not open!")
            
    def _validateEntry(self, entry):
        """
        Return the entry validity
        """
        
        return entry is not None and entry in self.__fd
            
    def _checkEntry(self):
        """
        Check that the current entry is valid
        """
        
        self._checkOpen()
        
        if not self._validateEntry(self.__entry):
            raise XPCSResultFileException(f"The entry {self.__entry} is not valid!")
            
    def _validateCorrKey(self, ckey):
        """
        Return the correlation key validity
        """
        
        return ckey is not None and ckey in self.__fd[f"/{self.__entry}/correlations"]
            
    def _checkCorrKey(self):
        """
        Check that the current correlation key is valid
        """
        
        self._checkEntry()
        
        if not self._validateCorrKey(self.__corrKey):
            raise XPCSResultFileException(f"The correlation key {self.__corrKey} is not valid!")
            
    def createEntry(self, entryto):
        """
        Create an entry to the result file from the raw file entry.
        
        :param rawfile: The raw file for which the new result entry is created
        :param entry: The entry to be created
        """
        
        self._checkOpen()
        
        if self._validateEntry(entryto):
            raise XPCSResultFileException(f"Entry {entryto} already exists in result file")
            
        if 'NX_class' not in self.__fd.attrs:
            self.__fd.attrs['NX_class'] = 'NXroot'
            self.__fd.attrs['creator'] = f"{sys.argv[0]} ({__name__})"
            self.__fd.attrs['host'] = os.uname()[1]
            self.__fd.attrs['default'] = f"/{entryto}"
            self.__fd.attrs['MagicString'] = "XPCSResultFile_v2.0"
            
        entry = self.__fd.create_group(entryto)
        entry.attrs.update({'NX_class': 'NXentry', 'default': 'time_averaged/azimuthal_average'})
            
        # Create the core structure (Only first level, user are requested to provide mandatory fields)
        self.__entry = entryto
        self.__corrKey = None
        
        # Acquisition: mandatory
        grp_acq = entry.create_group("acquisition")
        grp_acq.attrs.update({'NX_class': 'NXcollection'})
        
        # time_averaged: mandatory
        grp_saxs = entry.create_group("time_averaged")
        grp_saxs.attrs.update({'NX_class': 'NXcollection'})
        
        # parameters : mandatory
        grp_masks = entry.create_group("parameters")
        grp_masks.attrs.update({'NX_class': 'NXcollection'})
        
        # correlations: mandatory
        grp_correlations = entry.create_group("correlations")
        grp_correlations.attrs.update({'NX_class': 'NXcollection'})
        
        
    def setAcquisitionDict(self, params : dict):
        
        self._checkEntry()
        
        dsname = f"/{self.__entry}/acquisition"
                
        for k, v in params.items():
            self.__fd[dsname].create_dataset(k, data=v.encode())
        
        
    def setWorkingEntry(self, entry):
        """
        Set the current working entry
        
        :param entry: The new working entry
        """
        self._checkOpen()
        
        if self._validateEntry(entry):
            self.__entry = entry
            self.__corrKey = None
        else:
            raise XPCSResultFileException(f"Non-existent entry {entry}")
        
    def setSaxsCurve(self, q, I, err=None, q_unit=None, I_unit=None):
        """
        Set the SAXS curve
        
        :param q: vector of q
        :param I: I(q)
        :param err: Error on I
        :param force: Overwrite if SAXS curve already exists.
        """
        
        self._checkEntry()
        
        q = q.squeeze()
        I = I.squeeze()
        
        if err is not None:
            err = err.squeeze()
        
        if q.shape != I.shape or (err is not None and q.shape != err.shape):
            raise XPCSResultFileException("The q, I and err must have the same number of elements")
            
        if q.ndim > 1:
            raise ValueError("q is supposed to be 1 dim array.")
            
        if I.ndim == 1:
            I.shape = 1,*I.shape
        elif I.ndim > 2:
            raise ValueError("I must be 1 or 2 dim.")
            
        dsname = f"/{self.__entry}/time_averaged/azimuthal_average"
        nxdata = self.__fd.require_group(dsname)
        
        nxdata.attrs.update({'NX_class': 'NXdata',
                             'signal': 'Intensity',
                             'axes': [".","q"]})
        
        q_ds = self.__fd[dsname].require_dataset('q', q.shape, np.float32)
        q_ds.attrs['interpretation'] = 'scalar'
        
        if q_unit is not None:
            q_ds.attrs['unit'] = f'q_{q_unit}'
        elif 'unit' in q_ds.attrs:
            del q_ds.attrs['unit']
        
        q_ds[:] = q
        
        I_ds = self.__fd[dsname].require_dataset('Intensity', I.shape, np.float32)
        I_ds.attrs['interpretation'] = 'spectrum'
        
        if I_unit is not None:
            I_ds.attrs['unit'] = f'I_{I_unit}'
        elif 'unit' in I_ds.attrs:
            del I_ds.attrs['unit']
        
        I_ds[:] = I
        
        if err is not None:
            err_ds = self.__fd[dsname].require_dataset('err', err.shape, np.float32)
            err_ds.attrs['interpretation'] = 'scalar'
            
            err_ds[:] = err
            
            self.__fd[dsname].attrs['Intensity_errors'] = 'err'
        elif 'Intensity_errors' in self.__fd[dsname].attrs:
            del self.__fd[dsname].attrs['Intensity_errors']
            
    
    def add2DPattern(self, pattern, name, force=False):
        """
        Add a 2D pattern to the result file
        
        :param pattern: 2D pattern
        :param name: Name of the 2D pattern
        :param force: Erase the existing pattern if it already exists
        """
        
        self._checkEntry()
        
        if len(pattern.shape) < 2 or len(pattern.shape) > 3:
            raise XPCSResultFileException("Pattern must be an image or a stack of images")
        elif pattern.ndim == 2:
            cshape = None
        elif pattern.ndim == 3:
            cshape = (1,*pattern.shape[1:])
            
        dsname = f"/{self.__entry}/time_averaged"
        
        if force and name in self.__fd[dsname]:
            del self.__fd[f"{dsname}/{name}"]
        
        ds = self.__fd[dsname].create_dataset(name,
                                pattern.shape,
                                pattern.dtype,
                                pattern,
                                chunks=cshape,
                                compression="gzip")
        
        ds.attrs['interpretation'] = 'image'
        
    def add2DParameter(self, pattern, name, force=False):
        """
        Add a 2D pattern to the result file
        
        :param pattern: 2D pattern
        :param name: Name of the 2D pattern
        :param force: Erase the existing pattern if it already exists
        """
        
        self._checkEntry()
        
        if len(pattern.shape) < 2 or len(pattern.shape) > 3:
            raise XPCSResultFileException("Pattern must be an image or a stack of images")
        elif pattern.ndim == 2:
            cshape = None
        elif pattern.ndim == 3:
            cshape = (1,*pattern.shape[1:])
            
        dsname = f"/{self.__entry}/parameters"
        
        if force and name in self.__fd[dsname]:
            del self.__fd[f"{dsname}/{name}"]
        
        ds = self.__fd[dsname].create_dataset(name,
                                pattern.shape,
                                pattern.dtype,
                                pattern,
                                chunks=cshape,
                                compression="gzip")
        
        ds.attrs['interpretation'] = 'image'
        
    def addNumpyParameter(self, pattern, name):
        """
        Add a 2D pattern to the result file
        
        :param pattern: 2D pattern
        :param name: Name of the 2D pattern
        :param force: Erase the existing pattern if it already exists
        """
        
        self._checkEntry()
            
        dsname = f"/{self.__entry}/parameters"
        
        if name in self.__fd[dsname]:
            del self.__fd[f"{dsname}/{name}"]
        
        self.__fd[dsname].create_dataset(name,
                                pattern.shape,
                                pattern.dtype,
                                pattern,)
        
    def addStringParameter(self, name, value : str):
        """
        Add a 2D pattern to the result file
        
        :param pattern: 2D pattern
        :param name: Name of the 2D pattern
        :param force: Erase the existing pattern if it already exists
        """
        
        self._checkEntry()
            
        dsname = f"/{self.__entry}/parameters"
        
        if name in self.__fd[dsname]:
            del self.__fd[f"{dsname}/{name}"]
        
        self.__fd[dsname].create_dataset(name, data=value.encode())
        
    
    def addCorrelationKey(self, name, roi_masks, extras):
        """
        Create a derived correlation key name, and set as current one. The name will be suffixed with 4 digit number
        
        :param name: The name prefix to be created
        """
        
        dsname = f"/{self.__entry}/correlations/"
            
        if len(name) == 0:
            raise XPCSResultFileException("Name is empty!")
            
        ogrp = self.__fd[dsname].create_group(name)
        ogrp.attrs['NX_class'] = 'NXcollection'
        ogrp.attrs['default'] = 'g2'
        
        parameters = ogrp.create_group('parameters')
        parameters.attrs['NX_class'] = 'NXparameters'
        
        for k, _e in extras.items():
            if isinstance(_e, str):
                parameters.create_dataset(k, data=_e.encode())
            else:
                e = np.array(_e)
                
                if e.dtype == np.dtype("O"):
                    parameters.create_dataset(k, data=json.dumps(_e).encode())
                else:
                    parameters.create_dataset(k, e.shape, e.dtype, e)
            
        if roi_masks.ndim == 2:
            roi_masks.shape = 1, *roi_masks.shape
            
        if roi_masks.ndim != 3:
            raise ValueError("roi_masks must be a 3dim mask array")
            
        ogrp.create_dataset('roi_masks', roi_masks.shape, roi_masks.dtype, roi_masks, chunks=(1, *roi_masks.shape[1:]), compression='gzip')
        
        self.__corrKey = name
    
    def setWorkingCorrelationKey(self, ckey):
        """
        Set the current correlation
        
        :param ckey: Correlation key to be set as the current one
        """
        
        if self._validataCorrKey(ckey):
            self.__corrKey = ckey
        else:
            raise XPCSResultFileException(f"{ckey} is not a valid correlation key")
            
    def setCorrelationCurve(self, lag, cf, err=None):
        """
        Add a correlation curve to the current key
        
        :param lag: Lag time vector (M,)
        :param cf: Correlation function (N, M)
        :param err: Standard error (N, M)
        """
        
        self._checkCorrKey()
        
        dsname = f"/{self.__entry}/correlations/{self.__corrKey}/"
            
        grp = self.__fd[dsname].require_group('g2')
        grp.attrs['NX_class'] = 'NXdata'
        grp.attrs['signal'] = 'cf'
        grp.attrs['axes'] = ['.', 'lag']
        
        cf_ds = grp.require_dataset('cf', cf.shape, cf.dtype)
        cf_ds[:,:] = cf
        cf_ds.attrs['interpretation'] = 'spectrum'
        
        lag_ds = grp.require_dataset('lag', lag.shape, lag.dtype)
        lag_ds[:] = lag
        lag_ds.attrs['interpretation'] = 'scalar'
        lag_ds.attrs['axis'] = 2
        lag_ds.attrs['unit'] = 'tau_s'
        
        if err is not None:
            err_ds = grp.require_dataset('err', err.shape, err.dtype)
            err_ds[:,:] = err
            err_ds.attrs['interpretation'] = 'spectrum'
            grp.attrs['cf_uncertainty'] = 'err'
        
    def set2tCorrelation(self, lag, age, ttcf):
        """
        Add a two time correlation function to the current key.
        
        :param lag: Either a vector or a matrix with same dimension of ttcf
        :param age: Either a vector or a matrix with same dimension of ttcf
        :param ttcf: The two time correlation
        :param force: Allow overwriting
        """
        
        self._checkCorrKey()
        
        if ttcf.ndim != 3:
            raise XPCSResultFileException("TTCF must be a 3 dimensionnal array.")
            
        lag = np.squeeze(lag)
        age = np.squeeze(age)
        
        if lag.ndim == 1 and lag.shape[0] != ttcf.shape[2]:
            raise XPCSResultFileException(f"Lag is a vector. It's length must be the same length as 2nd dimension of a TTCF. {lag.shape} {ttcf.shape}")
        elif lag.ndim == 2 and lag.shape != ttcf.shape[1:]:
            raise XPCSResultFileException(f"Lag is a matrix. It's size must match dimension of TTCF. {lag.shape} {ttcf.shape}")
        elif lag.ndim > 2:
            raise XPCSResultFileException("Lag must have 1 or 2 dimension.")
        
        if age.ndim == 1 and age.shape[0] != ttcf.shape[1]:
            raise XPCSResultFileException(f"Age is a vector. It's length must be the same length as 1st dimension of a TTCF. {age.shape} {ttcf.shape}")
        elif age.ndim == 2 and age.shape != ttcf.shape[1:]:
            raise XPCSResultFileException(f"Age is a matrix. It's size must match dimension of TTCF. {age.shape} {ttcf.shape}")
        elif age.ndim > 2:
            raise XPCSResultFileException("Age must have 1 or 2 dimension.")
            
            
        dsname = f"/{self.__entry}/correlations/{self.__corrKey}"
        
            
        grp = self.__fd[dsname].require_group('twotime')
        grp.attrs['NX_class'] = 'NXdata'
        grp.attrs['ttcf_format'] = 'age,lag_time'
        grp.attrs['signal'] = 'ttcf'
        grp.attrs['axes'] = ['.', 'age', 'lag']
        
        cf_ds = grp.require_dataset('ttcf', ttcf.shape, ttcf.dtype, chunks=(1,*ttcf.shape[1:]), compression="gzip")
        cf_ds[:,:,:] = ttcf
        cf_ds.attrs['interpretation'] = 'image'
        
        lag_ds = grp.require_dataset('lag', lag.shape, lag.dtype)
        lag_ds[:] = lag
        lag_ds.attrs['interpretation'] = 'scalar'
        lag_ds.attrs['axis'] = 2
        lag_ds.attrs['unit'] = 'tau_s'
        
        age_ds = grp.require_dataset('age', age.shape, age.dtype)
        age_ds[:] = age
        age_ds.attrs['interpretation'] = 'scalar'
        age_ds.attrs['axis'] = 3
        age_ds.attrs['unit'] = 'age_s'
        
        
            
            
    
        
        
    
        
        
        


