# -*- coding: utf-8 -*-

import numpy as np
import logging, time

logger = logging.getLogger(__name__)

class Id02CommonCorrelator(object):
    
    def __init__(self, nframes):
        self.nframes = nframes
    
    def _time_binning_1D(self, _data, t, bins):
        
        data = _data.copy()
        msk = ~np.isnan(t)
        data[np.isnan(data)] = 0
        
        _sum,_ = np.histogram(t[msk], weights=data[msk], bins=bins)
        _N,_ = np.histogram(t[msk], bins=bins)
        
        with np.errstate(divide='ignore',invalid='ignore'):
            avg = _sum/_N
            avg[avg == 0] = np.nan
            return avg
        
        
    def _time_binning_2D(self, data, t, bins):
        r = None
        
        assert(data.shape == t.shape)
        
        for i in range(data.shape[0]):
            rd = self._time_binning_1D(data[i,:],t[i,:], bins)
            
            if r is None:
                r = np.zeros((data.shape[0], rd.shape[0]), dtype=np.float32)
                
            r[i,:] = rd
            
        return r
    
    
    def _correlate_init(self, frame_timing, lag_bins, age_bins, roi_masks):
        pass
    
    def _reduce_already_done(self):
        return False
    
    def _get_lag_age(self, frame_timing, lag_bins, age_bins):
        t0 = frame_timing[0]
        t1, t2 = np.meshgrid(frame_timing-t0, frame_timing-t0)
        Lag = t1 - t2
        Age = (t1 + t2)/2.
        
        nfram = len(frame_timing)
        
        olag = np.zeros((nfram,))
        oage = np.zeros((nfram,))
        
#        from matplotlib import pyplot as plt
#        plt.imshow(Age)
#        plt.show()
        
        _Age = np.fliplr(Age)
        
        
        rLag = np.empty_like(Lag)*np.nan
        rAge = np.empty_like(Lag)*np.nan
        
        rLag[:,0] = np.diag(Lag)
        rAge[:,0] = np.diag(Age)
        
        for i in range(nfram):
            olag[i] = np.mean(np.diag(Lag, i))
            oage[i] = np.mean(np.diag(_Age, nfram-i*2-1))
            
            if i > 0:
                rLag[i//2:-i//2,i] = np.diag(Lag, i)
                rAge[i//2:-i//2,i] = np.diag(Age, i)
            
            #logger.debug(np.diag(Lag, i))
        
        if lag_bins is None:
            lag = olag
        else:
            lag = self._time_binning_1D(olag, olag, lag_bins)
            
        if age_bins is None:
            age = oage
        else:
            age = self._time_binning_1D(oage, oage, age_bins)
            
        return olag, oage, rLag, rAge, lag, lag, age
        

    def correlate(self, frames, frame_timing, flatfield, cdata, roi_masks, lag_bins=None, age_bins=None):
        
        self._correlate_init(frame_timing, lag_bins, age_bins, roi_masks)
        
        logger.debug((frames.shape, roi_masks.shape))
        
        frames = frames.reshape((self.nframes, -1))
        roi_masks = roi_masks.reshape((roi_masks.shape[0],-1))
        
        logger.debug((frames.shape, roi_masks.shape))
        
        flatfield = flatfield.ravel()
        cdata = cdata.ravel()
        
        n_roi = roi_masks.shape[0]                
        
        trace = np.empty((n_roi, frames.shape[0], ), dtype=np.float32)*np.nan
        
        res, dev, ttcf = None, None, None
        
        for i in range(n_roi):
            mask = roi_masks[i,:]
            
            if not np.any(mask): # Skip if no pixels in this bin (May happen because of t-masking)
                continue
            
            t0 = time.time()
            tmpr, tmpd, tmptt = self._correlate(frames, mask)
            
            logger.debug((tmpr.shape, tmpd.shape, tmptt.shape))
            
            if res is None:
                
                olag, oage, oLag, oAge, lag, lagtt, age = self._get_lag_age(frame_timing, lag_bins, age_bins)
#                logger.debug((olag.shape, oage.shape, lag.shape, age.shape))
                
                if lagtt.ndim == 2:
                    mskltt = np.sum(~np.isnan(lagtt), axis=0) > 0
                    lagtt = lagtt[:,mskltt]
                    age = age[:,mskltt]
                else:
                    mskltt = ~np.isnan(lagtt)
                    lagtt = lagtt[mskltt]
                    
                mskl = ~np.isnan(lag)
                logger.debug((lag.shape, np.sum(mskl)))
                lag = lag[mskl]   
            
            
            logger.info("Correlator time %.2f seconds" , (time.time()-t0))
            
            correction=1
            fcorrection = (flatfield[mask]**2).mean()/flatfield[mask].mean()**2
            correction = (cdata[mask]**2).mean()/cdata[mask].mean()**2
            correction *= fcorrection
            
            logger.debug(f"fcorr: {fcorrection} corr: {correction}")
            
            tmpr /= correction
            tmpd /= correction
            tmptt /= correction
            
            
            if not self._reduce_already_done():
                if lag_bins is not None:
                    tmpr = self._time_binning_1D(tmpr, olag, lag_bins)
                    tmpd = self._time_binning_1D(tmpd, olag, lag_bins)
                    tmptt= self._time_binning_2D(tmptt, oLag, lag_bins)
                    
            if res is None:
                res = np.empty((n_roi, *tmpr[mskl].shape ), dtype=np.float32)*np.nan
                dev = np.empty((n_roi, *tmpd[mskl].shape ), dtype=np.float32)*np.nan
                ttcf = np.empty((n_roi, *tmptt[:,mskltt].shape), dtype=np.float32)*np.nan
                logger.debug(f"{tmptt.shape} {ttcf.shape}")
                    
             
            res[i,:]       = tmpr[mskl]
            dev[i,:]       = tmpd[mskl]
            trace[i,:]     = np.sum(frames[:,mask],1)
            ttcf[i,:,:]    = tmptt[:,mskltt]
        
        return res, dev, trace, lag, ttcf, lagtt, age
        
    
    



