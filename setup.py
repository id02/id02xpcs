#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct  5 17:12:34 2021

@author: opid02
"""

import sys
if sys.version_info < (3,6):
    sys.exit('Sorry, Python < 3.6 is not supported')


from setuptools import setup, find_packages

setup(name='id02xpcs',
      version='0.1',
      python_requires='>3.6',
      description='ESRF ID02 XPCS processing',
      author='William Chevremont',
      author_email='william.chevremont@esrf.fr',
      install_requires=[
          'numpy==1.21',
          'scipy',
          'h5py>=3.4',
          'hdf5plugin',
          'fabio',
          'silx',
          'scikit-cuda',
          'watchdog',
          'pyyaml',
          'pyopencl==2021.2.6', # WARNING: error with 2021.2.9
          #'dynamix @ https://github.com/williamchevremont/dynamix/tarball/yuriy'
      ],
      packages=find_packages(include=['id02xpcs', 'id02xpcs.*']),
      entry_points={
                  'console_scripts': ['id02xpcs_correlate=id02xpcs.bin.id02xpcs_correlate2:main',
                                    ]
              },
      package_data={'': ['det-config/*.ini', 'det-config/*.yaml']},
      include_package_data=True,
    )
