# -*- coding: utf-8 -*-

import logging, time
import numpy as np

logger = logging.getLogger(__name__)

#def py_dense_correlator(xpcs_data, mask, calc_std=False, ttcf_par=0):
#    """
#    Reference implementation of the dense correlator.
#    Parameters
#    -----------
#    xpcs_data: numpy.ndarray
#        Stack of XPCS frames with shape (n_frames, n_rows, n_columns)
#    mask: numpy.ndarray
#        Mask of bins in the format (n_rows, n_columns).
#        Zero pixels indicate unused pixels.
#    calc_std: boolean
#        Calculate the standard deviation in addition to the mean
#    
#    Return: 1 or 2 arrays depending on `calc_std`  
#    """
#    ind = np.where(mask > 0)  # unused pixels are 0
#    xpcs_data = np.array(xpcs_data[:, ind[0], ind[1]], np.float32)  # (n_tau, n_pix)
#    meanmatr = np.mean(xpcs_data, axis=1)  # xpcs_data.sum(axis=-1).sum(axis=-1)/n_pix
#    ltimes, lenmatr = np.shape(xpcs_data)  # n_tau, n_pix
#    meanmatr.shape = 1, ltimes
#
#    num = np.dot(xpcs_data, xpcs_data.T) / lenmatr
#    denom = np.dot(meanmatr.T, meanmatr)
#
#    res = np.zeros(ltimes-1)  # was ones()
#    dev = np.zeros_like(res)
#
#    for i in range(1,ltimes):  # was ltimes-1, so res[-1] was always 1 !
#        dia_n = np.diag(num, k=i) 
#        dia_d = np.diag(denom, k=i)
#        res[i-1] = np.sum(dia_n) / np.sum(dia_d)
#        if calc_std:
#            dev[i-1] = np.std(dia_n / dia_d) / np.sqrt(len(dia_d))
#    if ttcf_par>0:
#        trc = num/denom
#        tmp = np.diag(trc,k=1)
#        tmp = np.mean(tmp[tmp>0])
#        for j in range(ltimes):
#           trc[j,j]=tmp   
#        del tmp
#    else:
#        trc = 0
#    return CorrelationResult(res, dev, trc)

class CPUCorrelator(object):
    
    def setNFrames(self, n):
        pass

    def correlate(self, xpcs_data):
        
        timing = dict()
        t0 = time.perf_counter()
        xpcs_data = np.array(xpcs_data, np.float32)
        meanmatr = np.mean(xpcs_data, axis=1)  # xpcs_data.sum(axis=-1).sum(axis=-1)/n_pix
        ltimes, lenmatr = np.shape(xpcs_data)  # n_tau, n_pix
        
        logger.debug('%i %i'%(ltimes, lenmatr))
        
        meanmatr.shape = 1, ltimes
        
        timing['00_mean'] = time.perf_counter() - t0
    
        num = np.dot(xpcs_data, xpcs_data.T) / lenmatr
        denom = np.dot(meanmatr.T, meanmatr)
        
        timing['01_dots'] = time.perf_counter() - t0
    
        res = np.zeros(ltimes-1)  # was ones()
        dev = np.zeros_like(res)
    
        for i in range(1,ltimes):
            dia_n = np.diag(num, k=i) 
            dia_d = np.diag(denom, k=i)
            res[i-1] = np.sum(dia_n) / np.sum(dia_d)
            dev[i-1] = np.std(dia_n / dia_d) / np.sqrt(len(dia_d))
            
        timing['02_res/dev'] = time.perf_counter() - t0    
            
        trc = num/denom
        
        timing['03_trc'] = time.perf_counter() - t0
        timing['total'] = time.perf_counter() - t0
        
        return timing, (res, dev, trc)


