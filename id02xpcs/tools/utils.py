# -*- coding: utf-8 -*-


import os
import fabio
import numpy as np

def updiv(a, b):
    """
    return the integer division, plus one if `a` is not a multiple of `b`
    """
    return int((a + (b - 1)) // b)

class FileCache():
    
    __db = {}
    #__recentfiles = []
    #MAXFILES = 10
    
    @classmethod
    def getDataFromFile(cls, filename):
        
        if not os.path.isfile(filename):
            raise ValueError(f"{filename} do not exists.")
        
        filename = os.path.realpath(filename)
        
        if filename not in cls.__db:
            
            with fabio.open(filename) as f:
                data = f.data
            
                cls.__db[filename] = {'data': data,
                                      'time': os.path.getmtime(filename)}
                
        elif cls.__db[filename]['time'] < os.path.getmtime(filename): # Refresh the cache, file did change
            del cls.__db[filename]
            return cls.getDataFromFile(filename)    
            
        
        
        return cls.__db[filename]['data']
        


