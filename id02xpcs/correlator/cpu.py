#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct  6 12:14:49 2021

@author: opid02
"""


import numpy as np
import logging

from .common import Id02CommonCorrelator

logger = logging.getLogger(__name__)

class Id02CPUCorrelator(Id02CommonCorrelator):
    """
    This is a reimplementation of MatMulCorrelator, which also perform baseline correction and 
    give access to all ttcf.
    
    Results are saved in the output h5 file
    """
    def __init__(self, nframes):
        super().__init__(nframes)
        
    def py_dense_correlator(xpcs_data, mask, calc_std=False, ttcf_par=0):
        """
        Reference implementation of the dense correlator.
        Parameters
        -----------
        xpcs_data: numpy.ndarray
            Stack of XPCS frames with shape (n_frames, n_rows, n_columns)
        mask: numpy.ndarray
            Mask of bins in the format (n_rows, n_columns).
            Zero pixels indicate unused pixels.
        calc_std: boolean
            Calculate the standard deviation in addition to the mean
        
        Return: 1 or 2 arrays depending on `calc_std`  
        """
        ind = np.where(mask > 0)  # unused pixels are 0
        xpcs_data = np.array(xpcs_data[:, ind[0], ind[1]], np.float32)  # (n_tau, n_pix)
        meanmatr = np.mean(xpcs_data, axis=1)  # xpcs_data.sum(axis=-1).sum(axis=-1)/n_pix
        ltimes, lenmatr = np.shape(xpcs_data)  # n_tau, n_pix
        meanmatr.shape = 1, ltimes
    
        num = np.dot(xpcs_data, xpcs_data.T) / lenmatr
        denom = np.dot(meanmatr.T, meanmatr)
    
        res = np.zeros(ltimes)  # was ones()
        if calc_std:
            dev = np.zeros_like(res)
    
        for i in range(ltimes):  # was ltimes-1, so res[-1] was always 1 !
            dia_n = np.diag(num, k=i) 
            dia_d = np.diag(denom, k=i)
            res[i-1] = np.sum(dia_n) / np.sum(dia_d)
            if calc_std:
                dev[i-1] = np.std(dia_n / dia_d) / np.sqrt(len(dia_d))
                
        if ttcf_par>0:
            trc = num/denom
            tmp = np.diag(trc,k=1)
            tmp = np.mean(tmp[tmp>0])
            for j in range(ltimes):
               trc[j,j]=tmp   
            del tmp
        else:
            trc = 0
            
        return res, dev, trc    
    
    def _correlate(self, frames, mask):
        res, dev, trc = self.py_dense_correlator(frames, mask, True, 1)
        
        # Tilt ttcf
        t_ttcf = np.empty(trc.shape,trc.dtype)*np.nan
        t_ttcf[:,0] = np.diag(trc)
        for d in range(trc.shape[0]):
            t_ttcf[d//2:-d//2,d] = np.diag(trc,d)
            
        return res, dev, trc
  
            
            


